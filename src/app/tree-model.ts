export class treemodal{
    field:string="";
    headerText:string="";
    textAlign:string="";
    format:string="";
    width:string="";
    editTypes:string = "";
    freeze:string = "";
    filter:string = "";
    checked:boolean = false;
    isPrimaryKey:boolean = false;
    minWidth: number;
    maxWidth: number;
}