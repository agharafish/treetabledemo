import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { DomSanitizer } from '@angular/platform-browser';
import {
    TreeGridComponent,
    PageSettingsModel,
    SortSettingsModel,
    EditSettingsModel,
    SelectionSettingsModel,
    FilterSettingsModel,
    TreeGridPdfExportProperties,
    TreeGridExcelExportProperties,
    Column as TreeGridColumn

} from "@syncfusion/ej2-angular-treegrid";
import { treemodal } from "./tree-model";
import { ChangeEventArgs } from '@syncfusion/ej2-dropdowns';
import { ActionEventArgs, Column, RowInfo, freezeDirection, RowSelectEventArgs, BeforeCopyEventArgs } from '@syncfusion/ej2-grids';
import { DropDownListComponent } from "@syncfusion/ej2-angular-dropdowns";
import { ButtonComponent, CheckBoxComponent } from "@syncfusion/ej2-angular-buttons";
import { ButtonPropsModel, DialogComponent, DialogUtility } from "@syncfusion/ej2-angular-popups";
import { BeforeOpenCloseEventArgs } from "@syncfusion/ej2-inputs";
import { ClickEventArgs, MenuEventArgs } from "@syncfusion/ej2-navigations";
import { getValue, isNullOrUndefined, Position } from "@syncfusion/ej2-base";
import { freezemodal } from "./freeze-model";
import { datamodal } from "./data-model";
import { ColorPickerComponent, ColorPickerEventArgs, NumericTextBoxComponent } from "@syncfusion/ej2-angular-inputs";
import { Data } from "@angular/router";
import { SharedServicesService } from "./shared-services.service";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    public data: Object[];
    public lstnewdata: Object[];

    public d1data: Object;
    public d3data: Object[];
    public objdatamodel: datamodal;

    public stylesApplied: Styles[] = [];
    public gridclass: string = "";
    public ddlfields: Object;
    public d2data: Object;
    public fields: Object;
    public d4Allignment: Object;

    public d5ColumnDataTypes: Object;
    public textwrapddl: Object;
    IntegralUITreeGrid
    public content: string = 'Atleast one Column should be in movable';
    public header: string = 'Frozen';
    public showCloseIcon: boolean = false;
    public target: string = '.control-section';
    public width: string = '300px';
    public showControls: boolean = false;
    public minWidthofSelectedColumn: number = 120;
    public selectedColumnHeaderDataType: Object;
    public selectedColumnHeaderFont: Object;
    public treeGridColumns: Column[];
    public selectedColumn: TreeGridColumn;
    public selectedColumnHeader: string = 'taskid';
    public selectedWrapperSettings: string = 'Both';
    public defaultDatatypeSelected: string = 'defaultedit';
    public defaultAlignment: string = 'center';
    public defaultTextWrap: string = "Both";
    public defaultcellBgColor: string = '#fff';
    public defaultcellFontSize: string = 12 + "px";
    public defaultcellColor: string = '#808080';
    public textWrapDefault: string = 'both';

    @ViewChild('treegrid', { static: false }) public treegrid: TreeGridComponent;
    @ViewChild('dropdown1', { static: false }) public dropdown1: DropDownListComponent;
    @ViewChild('dropdown2', { static: false }) public dropdown2: DropDownListComponent;
    @ViewChild('dropdown3', { static: false }) public dropdown3: DropDownListComponent;
    @ViewChild('textwrapdropdown', { static: false }) public textwrapdropdown: DropDownListComponent;

    @ViewChild('textWrapCheckbox', { static: false }) public textWrapCheckbox: CheckBoxComponent;
    @ViewChild('minimumWidthInput', { static: false }) public minimumWidthInput: NumericTextBoxComponent;
    @ViewChild('fontsize', { static: false }) public fontsize: NumericTextBoxComponent;
    @ViewChild('fontColor', { static: false }) public fontColor: ColorPickerComponent;
    @ViewChild('BgColor', { static: false }) public BgColor: ColorPickerComponent;

    @ViewChild('button1', { static: false }) public button1: ButtonComponent;
    @ViewChild('button2', { static: false }) public button2: ButtonComponent;
    @ViewChild('columndropdown', { static: false }) public columnDropDown: DropDownListComponent;
    @ViewChild('directiondropdown', { static: false }) public directionDropDown: DropDownListComponent;
    @ViewChild('alertDialog', { static: true }) public alertDialog: DialogComponent;
    @ViewChild('taskID', { static: false }) public taskID: CheckBoxComponent;
    @ViewChild('taskName', { static: false }) public taskName: CheckBoxComponent;
    @ViewChild('startDate', { static: false }) public startDate: CheckBoxComponent;
    @ViewChild('duration', { static: false }) public duration: CheckBoxComponent;
    timeoutHandler: number;
    public lstTreemodal: treemodal[];
    public lstdatamodal: datamodal[];
    public cruddatamodel: datamodal[];
    public objTreemodal: treemodal;
    public objAddTreemodal: treemodal;
    public sortSettings: SortSettingsModel;
    public pageSettings: PageSettingsModel;
    public editSettings: EditSettingsModel;
    public filterSettings: FilterSettingsModel
    public selectionOptions: SelectionSettingsModel;
    public frozen: string = "2";
    public toolbar: Object[];
    title = 'appBootstrap';
    public test: any;
    public collapseStatePersist: boolean = true;
    closeResult: string;
    public refresh: boolean = true;
    public multiselect: boolean =false;

    public datalist: Object[];

    selectedRow: string = "";

    public trRow;
    public rowDetails;


    public directionData: Object[] = [
        { id: 'Left', name: 'Left' },
        { id: 'Right', name: 'Right' },
        { id: 'Center', name: 'Center' }
    ];

    public animationSettings: object = { effect: 'None' };
    public visible: boolean = false;
    public contextMenuItems: object;
    public ShowHideColumns: boolean = false;
    public FreezeColumns: boolean = false;
    public FilterColumns: boolean = false;
    public MultiSortColumns: boolean = false;
    public StyleColumns: boolean = false;
    public NewColumns: boolean = false;

    public Addcol: boolean = false;
    public Editcol: boolean = false;
    public Delcol: boolean = false;
    public objfreeze: freezemodal;
    public frozenColumn: number;
    public alertHeader: string = 'Copy with Header';
    public hidden: Boolean = false;
    public alertWidth: string = '300px';
    public alertContent: string = 'Atleast one row should be selected to copy with header';
    public rows: object[];
    parentData: any;
    public newChlindRow: any[];
    public newmodel: datamodal;
    public bgColorSwitch: string = "Pink";
    public copiedrowsIndexes: any[] = [];
    public cutRows: boolean = false;
    public resizeSettings = { mode: "Auto" };
    constructor(private sanitizer: DomSanitizer, private _svc: SharedServicesService) {
        this.objTreemodal = new treemodal();
        this.lstTreemodal = [];
        this.objAddTreemodal = new treemodal();
        this.objfreeze = new freezemodal();
        this.lstdatamodal = [];
        this.objdatamodel = new datamodal();
        this.newChlindRow = [];
        this.cruddatamodel = [];
        this.copiedrowsIndexes = [];
    }

    ngOnInit(): void {

        this.getColumns();
        this.getRecord();
        this.getColumnStyles();
        this.d4Allignment = [
            { id: 'Center', name: 'Center' },
            { id: 'Right', name: 'Right' },
            { id: 'Left', name: 'Left' },
            { id: 'Justify', name: 'Justify' },
        ],
            this.textwrapddl = [
                {
                    id: "both", name: 'Both'

                },
                {
                    id: "header", name: 'header'

                },
                {
                    id: "content", name: 'content'

                },
            ];
        this.d5ColumnDataTypes = [
            { id: 'defaultedit', name: 'Text' },
            { id: 'numericedit', name: 'Number' },
            { id: 'datepickeredit', name: 'Date' },
            { id: 'booleanedit', name: 'Boolean' },
            { id: 'dropdownedit', name: 'Dropdown' }


            // { id: 'datetimepickeredit', name: 'Datetime picker' },
        ];
        this.lstTreemodal = [];
        this.lstdatamodal = [];
        this.newChlindRow = [];
        this.copiedrowsIndexes = [];
        this.objAddTreemodal = new treemodal();
        this.objTreemodal = new treemodal();
        this.objfreeze = new freezemodal();
        this.objdatamodel = new datamodal();
        this.cruddatamodel = [];
        this.filterSettings = { type: 'FilterBar', hierarchyMode: 'Parent', mode: 'Immediate' };
        this.selectionOptions = { persistSelection: true, type: "Single" };
        this.toolbar = [{ text: 'Copy', tooltipText: 'Copy', prefixIcon: 'e-copy', id: 'copy' },
        { text: 'Cut', tooltipText: 'Cut', prefixIcon: 'e-cut', id: 'cut' }];
        this.d3data = [];
        this.sortSettings = {
            columns: [{ field: 'taskID', direction: 'Ascending' },
            { field: 'taskName', direction: 'Ascending' }]
        }
        this.pageSettings = { pageSize: 6, pageCount: 2, pageSizes: true, pageSizeMode: 'All' }
        this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, showDeleteConfirmDialog: false, mode: 'Dialog' };
        this.ddlfields = { text: 'name', value: 'id' };
        this.fields = { text: 'name', value: 'id' }
        
        this.contextMenuItems = [
            { text: 'Style', target: '.e-headercontent', id: 'style' },
            { text: 'New', target: '.e-headercontent', id: 'new' },
            { text: 'Show', target: '.e-headercontent', id: 'show' },
            { text: 'Freeze', target: '.e-headercontent', id: 'freeze' },
            { text: 'Filter', target: '.e-headercontent', id: 'filter' },
            { text: 'Multi-Sort', target: '.e-headercontent', id: 'multisort' },
            { text: 'New Row', target: '.e-content', id: 'newrow' },
            { text: 'Delete Row', target: '.e-content', id: 'deleterow' },
            { text: 'Multi-Select', target: '.e-content', id: 'multiselectrow' },
            { text: 'Paste Sibling', target: '.e-content', id: 'pastesibling' },
            { text: 'Paste Child', target: '.e-content', id: 'pastechild' }

        ]
       
    }
    public change(e: ChangeEventArgs): void {
        let columnName: string = <string>this.dropdown1.value;
        let toColumnIndex: number = <number>e.value;
        this.treegrid.reorderColumns(columnName, (<Column>this.treegrid.columns[toColumnIndex]).field);
    }
    public onChange(e: ChangeEventArgs): void {
        let columnName: string = <string>e.value;
        let index: number = this.treegrid.getColumnIndexByField(columnName);
        this.dropdown2.value = index.toString();
    }
    columnHide(e: ChangeEventArgs): void {
        let columnName: string = <string>e.value;
        let column = this.treegrid.getColumnByField(columnName);
        if (column.visible === undefined || column.visible) {
            this.button2.disabled = true;
            this.button1.disabled = false;
        } else {
            this.button1.disabled = true;
            this.button2.disabled = false;
        }
    }
    ShowHide(e: ChangeEventArgs): void {
        let columnName: string = <string>e.value;
        let column = this.treegrid.getColumnByField(columnName);
        if (column.visible === undefined || column.visible) {
            this.button2.disabled = true;
            this.button1.disabled = false;
        } else {
            this.button1.disabled = true;
            this.button2.disabled = false;
        }
    }
    public actionComplete(args: ActionEventArgs): void {
        if (args.requestType === 'reorder') {
            let columnName: string = <string>this.dropdown1.value;
            let index: number = this.treegrid.getColumnIndexByField(columnName);
            this.dropdown2.value = index.toString();
            this.applyStyleChangesToDOM();
        }
        if (args.requestType === 'sorting') {
            for (let columns of this.treegrid.getColumns()) {
                for (let sortcolumns of this.treegrid.sortSettings.columns) {
                    if (sortcolumns.field === columns.field) {
                        this.check(sortcolumns.field, true); break;
                    } else {
                        this.check(columns.field, false);
                    }
                }
            }
            this.applyStyleChangesToDOM();
        }
        if (args.requestType == "save" || args.requestType == "delete") {

            this.saveRecord(this.cruddatamodel);

            this.applyStyleChangesToDOM();
        }
        if (args.requestType == "refresh") {
            this.applyStyleChangesToDOM();
        }
        if (args.requestType == "filtering") {
            this.applyStyleChangesToDOM();
        }


    }
    public clicked(e: MouseEvent): void {
        let columnName: string = <string>this.dropdown3.value;
        let column = this.treegrid.getColumnByField(columnName);
        let hiddenColumns: HTMLTextAreaElement = document.getElementById('hiddencolumns') as HTMLTextAreaElement;
        this.treegrid.grid.showColumns(column.headerText, 'headerText');
        this.button2.disabled = true;
        this.button1.disabled = false;
        hiddenColumns.value = hiddenColumns.value.replace(column.headerText + '\n', '');
    }
    onClicked(): void {
        let columnName: string = <string>this.dropdown3.value;
        let column = this.treegrid.getColumnByField(columnName);
        let hiddenColumns: HTMLTextAreaElement = document.getElementById('hiddencolumns') as HTMLTextAreaElement;

        if (this.treegrid.getHeaderTable().querySelectorAll('th.e-hide').length === 3) {
            alert('Atleast one Column should be visible');
        } else {
            this.treegrid.grid.hideColumns(column.headerText, 'headerText');
            this.button1.disabled = true;
            this.button2.disabled = false;
            hiddenColumns.value = hiddenColumns.value + column.headerText + '\n';
        }
    };
    public columnChange(e: ChangeEventArgs): void {
        let columnName: string = this.objfreeze.field as string;
        let column: Column = this.treegrid.grid.getColumnByField(columnName);
        let value: string = column.freeze === undefined ? 'Center' : column.freeze;
        this.refresh = this.directionDropDown.value === value;
        this.directionDropDown.value = value;
    }
    public directionChange(e: ChangeEventArgs): void {
        if (this.refresh) {
            let columnName: string = this.objfreeze.field as string;
            let mvblColumns: Column[] = this.treegrid.grid.getMovableColumns();
            if (
                mvblColumns.length === 1 &&
                columnName === mvblColumns[0].field &&
                e.value !== mvblColumns[0].freeze
            ) {
                this.alertDialog.show();
                this.refresh = false;
                this.directionDropDown.value = 'Center';
                this.directionDropDown.refresh();
            } else {
                this.treegrid.grid.getColumnByField(columnName).freeze =
                    e.value === 'Center' ? undefined : (e.value as freezeDirection);
                for (var i = 0; this.lstTreemodal.length > i; i++) {
                    if (this.lstTreemodal[i].field == this.objfreeze.field) {
                        this.lstTreemodal[i].freeze = e.value.toString();
                        break;
                    }
                }
                this.treegrid.refreshColumns();
            }
        }
        this.refresh = true;
    }
    public alertDialogBtnClick = (): void => {
        this.alertDialog.hide();
    };
    public dlgButtons: ButtonPropsModel[] = [
        {
            click: this.alertDialogBtnClick.bind(this),
            buttonModel: { content: 'OK', isPrimary: true }
        }
    ];
    public onClick1(e: any, field: string): void {
        for (var i = 0; this.lstTreemodal.length > i; i++) {
            if (this.lstTreemodal[i].field == field) {
                if (this.lstTreemodal[i].checked) {
                    this.treegrid.sortByColumn(this.lstTreemodal[i].field, 'Ascending', true);
                }
                else {
                    this.treegrid.grid.removeSortColumn(this.lstTreemodal[i].field);
                }
            }
        }
        this.treegrid.refreshHeader();

    }
    public check(field: string, state: boolean): void {
        for (var i = 0; this.lstTreemodal.length > i; i++) {
            if (this.lstTreemodal[i].field == field) {
                this.lstTreemodal[i].checked = state;
                break;
            }
        }
    }
    contextMenuOpen(arg?: BeforeOpenCloseEventArgs): void {
        let elem: Element = arg.event.target as Element;
        let row: Element = elem.closest('.e-row');
        let uid: string = row && row.getAttribute('data-uid');
        let items: Array<HTMLElement> = [].slice.call(document.querySelectorAll('.e-menu-item'));
        for (let i: number = 0; i < items.length; i++) {
            if(items[i].innerText == 'Style')
            {
                if(this.StyleColumns)
                {
                    items[i].style.backgroundColor = 'Pink'
                }
                else
                {
                    items[i].style.backgroundColor = 'White'
                }
            }
            if(items[i].innerText == 'New')
            {
                if(this.NewColumns)
                {
                    items[i].style.backgroundColor = 'Pink'
                }
                else
                {
                    items[i].style.backgroundColor = 'White'
                }
            }
            if(items[i].innerText == 'Show')
            {
                if(this.ShowHideColumns)
                {
                    items[i].style.backgroundColor = 'Pink'
                }
                else
                {
                    items[i].style.backgroundColor = 'White'
                }
            }
            if(items[i].innerText == 'Freeze')
            {
                if(this.FreezeColumns)
                {
                    items[i].style.backgroundColor = 'Pink'
                }
                else
                {
                    items[i].style.backgroundColor = 'White'
                }
                
            }
            if(items[i].innerText == 'Filter')
            {
                if(this.FilterColumns)
                {
                    items[i].style.backgroundColor = 'Pink'
                }
                else
                {
                    items[i].style.backgroundColor = 'White'
                }
            }
            if(items[i].innerText == 'Multi-Sort')
            {
                if(this.MultiSortColumns)
                {
                    items[i].style.backgroundColor = 'Pink'
                }
                else
                {
                    items[i].style.backgroundColor = 'White'
                }
            }
            if(items[i].innerText == 'Multi-Select')
            {
                if(this.multiselect)
                {
                    items[i].style.backgroundColor = 'Pink'
                }
                else
                {
                    items[i].style.backgroundColor = 'White'
                }
            }
        }
        // if (elem.closest('.e-row')) {
        //     if (isNullOrUndefined(uid) ||
        //         isNullOrUndefined(getValue('hasChildRecords', this.treegrid.grid.getRowObjectFromUID(uid).data))) {
        //         arg.cancel = true;
        //     } else {
        //         let flag: boolean = getValue('expanded', this.treegrid.grid.getRowObjectFromUID(uid).data);
        //         let val: string = flag ? 'none' : 'block';
        //         document.querySelectorAll('li#expandrow')[0].setAttribute('style', 'display: ' + val + ';');
        //         val = !flag ? 'none' : 'block';
        //         document.querySelectorAll('li#collapserow')[0].setAttribute('style', 'display: ' + val + ';');
        //     }
        // } else {
        //     let len = this.treegrid.element.querySelectorAll('.e-treegridexpand').length;
        //     if (len !== 0) {
        //         document.querySelectorAll('li#collapseall')[0].setAttribute('style', 'display: block;');
        //     } else {
        //         document.querySelectorAll('li#expandall')[0].setAttribute('style', 'display: block;');
        //     }
        // }
    }

    contextMenuClick(args?: ContextMenuArgs): void {
        if (args.item.id === 'style') {
            if (this.StyleColumns) {
                this.StyleColumns = false;
            }
            else {
                this.d3data = [];
                for (var i = 0; this.lstTreemodal.length > i; i++) {
                    this.d3data.push(this.lstTreemodal[i].field)
                }
                this.StyleColumns = true;
                /*document.getElementById('treeGridDiv').setAttribute('class', 'col-md-8')*/
                /* this.treeGridColumns = <Column[]>this.treegrid.columns;*/
                //this.selectedColumn = args.column;
                //this.selectedColumnHeader = args.column.field;
                //debugger;
                //let styliesAlreadyApplied = this.stylesApplied.filter(x => x.columnField == args.column.field);
                //if (styliesAlreadyApplied != undefined && styliesAlreadyApplied.length > 0) {
                //    this.defaultDatatypeSelected = styliesAlreadyApplied[0].dataType;
                //    this.defaultcellFontSize = styliesAlreadyApplied[0].fontSize.replace("px","");
                //    this.defaultcellColor = styliesAlreadyApplied[0].fontColor;
                //    this.defaultcellBgColor = styliesAlreadyApplied[0].bgColor;
                //    this.defaultAlignment = styliesAlreadyApplied[0].alignment;
                //    this.defaultTextWrap = styliesAlreadyApplied[0].wrap;
                //    this.minWidthofSelectedColumn = styliesAlreadyApplied[0].minimumWidth;

                //} else {
                //    this.minWidthofSelectedColumn = <number>args.column.width;
                //    this.defaultDatatypeSelected = args.column.editType == "" ? "defaultedit" : args.column.editType.toString();
                //    this.defaultAlignment = args.column.textAlign.toString() == "" ? 'Center' : args.column.textAlign.toString();
                //    this.defaultcellFontSize = '12';
                //    this.defaultcellColor = '#000000';
                //    this.defaultcellBgColor ='#FFFFFF';
                //    this.defaultTextWrap = 'both';

                //}
                this.columnHeaderSelected(args.column);

            }
            this.ShowHideColumns = false;
            this.FreezeColumns = false;
            this.FilterColumns = false;
            this.MultiSortColumns = false;
            this.NewColumns = false;
        }
        else if (args.item.id === 'new') {
            if (this.NewColumns) {
                this.NewColumns = false;
            }
            else {
                this.NewColumns = true;
            }
            this.ShowHideColumns = false;
            this.FreezeColumns = false;
            this.FilterColumns = false;
            this.MultiSortColumns = false;
            this.StyleColumns = false;
        }
        else if (args.item.id === 'show') {
            if (this.ShowHideColumns) {
                this.ShowHideColumns = false;
            }
            else {
                this.ShowHideColumns = true;
            }
            this.FreezeColumns = false;
            this.FilterColumns = false;
            this.MultiSortColumns = false;
            this.StyleColumns = false;
            this.NewColumns = false;
        }
        else if (args.item.id === 'freeze') {
            if(this.FreezeColumns)
            {
                this.frozenColumn = 0;
                this.FreezeColumns = false;
            }
            else
            {
                this.FreezeColumns = true;
                this.frozenColumn = this.treegrid.getColumnIndexByField(args.column.field) + 1;
                //this.treegrid.refreshColumns();

            }
            
            this.ShowHideColumns = false;
            this.FilterColumns = false;
            this.MultiSortColumns = false;
            this.StyleColumns = false;
            this.NewColumns = false;
        }
        else if (args.item.id === 'multisort') {
            if (this.MultiSortColumns) {
                this.MultiSortColumns = false;
            }
            else {
                this.MultiSortColumns = true;
            }
            this.ShowHideColumns = false;
            this.FreezeColumns = false;
            this.FilterColumns = false;
            this.StyleColumns = false;
            this.NewColumns = false;
        }
        else if (args.item.id === 'filter') {
            if (this.FilterColumns) {
                this.FilterColumns = false;
            }
            else {
                this.FilterColumns = true;
            }
            this.ShowHideColumns = false;
            this.FreezeColumns = false;
            this.MultiSortColumns = false;
            this.StyleColumns = false;
            this.NewColumns = false;
        }
        else if (args.item.id == 'multiselectrow') {
            if(this.multiselect)
        {
            this.selectionOptions = { persistSelection: true, type: "Single" };
            this.multiselect = false;
        }
        else
        {
            this.multiselect = true;
            this.selectionOptions = { persistSelection: true, type: "Multiple" };
        }
            
        }
        else if (args.item.id == 'newrow') {
            if (this.treegrid.getSelectedRows().length > 0) {
                this.treegrid.addRecord("", Number(this.treegrid.selectedRowIndex), "Below");
            }
            else {
                this.treegrid.addRecord("", 0, "Below");
            }

        }
        else if (args.item.id == 'editrow') {
            if (this.treegrid.getSelectedRows().length > 0) {
                this.treegrid.updateRow(Number(this.selectedRow), "");
            }
            else {
                this.treegrid.updateRow(0, "");
            }


        }
        else if (args.item.id == 'deleterow') {
            this.parentData = args.rowInfo.rowData;
            //this.treegrid.deleteRow(this.trRow);
            if (this.parentData.parentItem != undefined) {
                this.lstdatamodal[this.parentData.parentItem.index].subtasks.splice(args.rowInfo.rowIndex, 1);
            }
            else {
                this.lstdatamodal.splice(args.rowInfo.rowIndex, 1);
            }

            //this.cruddatamodel.splice(args.rowInfo.rowIndex,1);
            this.saveRecord(this.lstdatamodal);
        }
        else if (args.item.id == 'pastesibling') {
            this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, showDeleteConfirmDialog: false, mode: 'Dialog' };
            if(this.copiedrowsIndexes.length > 0)
            {
                for(var i = 0;this.copiedrowsIndexes.length > i;i++)
                {
                    //this.cruddatamodel.push(this.cruddatamodel[this.copiedrowsIndexes[i]]);
                    this.treegrid.addRecord(this.cruddatamodel[this.copiedrowsIndexes[i]], this.copiedrowsIndexes[i], "Below")
                    if(this.cutRows)
                    {
                        this.cruddatamodel.splice(this.copiedrowsIndexes[i],1);
                    }
                }
            }
            

        }
        else if (args.item.id == 'pastechild') {
            this.parentData = args.rowInfo.rowData;
            this.editSettings = {
                allowEditing: true, allowAdding: true, allowDeleting: true, showDeleteConfirmDialog: false, mode: 'Dialog',
                newRowPosition: 'Child'
            };
            //this.treegrid.addRecord(args.rowInfo.rowData, args.rowInfo.rowIndex, "Below")
            // for(var i =0;this.cruddatamodel.length > i;i++)
            // {
            //     if(this.lstdatamodal[i].taskData != undefined)
            //     {
            //         this.cruddatamodel[args.rowInfo.rowIndex].subtasks[0] = this.lstdatamodal[i].taskData
            //         // if(this.lstdatamodal[i].subtasks.length == 1)
            //         // {
            //         //     this.lstdatamodal[i].subtasks[0].subtasks = [];
            //         // }
            //     }
            // }
            if (this.parentData.parentItem != undefined) {
                // this.newChlindRow = [];
                // this.newmodel = new datamodal();
                // this.test = args.rowInfo.rowData;
                // this.newmodel.taskID = this.test.taskID
                // this.newmodel.taskName = this.test.taskName
                // this.newmodel.duration = this.test.duration
                // this.newmodel.startDate = this.test.startDate
                // this.newmodel.endDate = this.test.endDate
                // this.newmodel.progress = this.test.progress
                // this.newChlindRow[0] = this.newmodel;
                if(this.copiedrowsIndexes.length > 0)
                {
                    for(var i = 0;this.copiedrowsIndexes.length > i;i++)
                    {
                        for (var j = 0; this.cruddatamodel.length > j; j++) {
                            if (this.cruddatamodel[j].taskID == this.parentData.parentItem.taskID) {
                                this.newChlindRow = [];
                                this.newmodel = new datamodal();
                                this.test = this.cruddatamodel[this.copiedrowsIndexes[i]]
                                this.newmodel.taskID = this.test.taskID
                                 this.newmodel.taskName = this.test.taskName
                                this.newmodel.duration = this.test.duration
                                this.newmodel.startDate = this.test.startDate
                                this.newmodel.endDate = this.test.endDate
                                this.newmodel.progress = this.test.progress
                                this.newChlindRow[0] = this.newmodel;
                                this.cruddatamodel[j].subtasks.push(this.newChlindRow[0]);
                                if(this.cutRows)
                                {
                                    this.cruddatamodel.splice(this.copiedrowsIndexes[i],1);
                                }
                            }
                        }
                    }
                }

                
                // for (var j = 0; this.cruddatamodel.length > j; j++) {
                //     if (this.cruddatamodel[j].taskID == this.parentData.parentItem.taskID) {
                //         this.cruddatamodel[j].subtasks.push(this.newChlindRow[0]);
                //     }
                // }

            }
            else {
                for (var i = 0; this.cruddatamodel.length > i; i++) {
                    // if (this.cruddatamodel[i].taskID == this.parentData.taskID) {
                        if (this.cruddatamodel[i].taskID == this.parentData.taskID) {
                        if (this.cruddatamodel[i].subtasks == undefined) {
                            this.cruddatamodel[i].subtasks = [];
                            // this.newChlindRow = [];
                            // this.newmodel = new datamodal();
                            // this.test = args.rowInfo.rowData;
                            // this.newmodel.taskID = this.test.taskID
                            // this.newmodel.taskName = this.test.taskName
                            // this.newmodel.duration = this.test.duration
                            // this.newmodel.startDate = this.test.startDate
                            // this.newmodel.endDate = this.test.endDate
                            // this.newmodel.progress = this.test.progress
                            // this.newChlindRow[0] = this.newmodel;
                            if(this.copiedrowsIndexes.length > 0)
                            {
                                for(var j = 0;this.copiedrowsIndexes.length > j;j++)
                                {
                                    this.newChlindRow = [];
                                    this.newmodel = new datamodal();
                                    this.test = this.cruddatamodel[this.copiedrowsIndexes[j]]
                                    this.newmodel.taskID = this.test.taskID
                                     this.newmodel.taskName = this.test.taskName
                                    this.newmodel.duration = this.test.duration
                                    this.newmodel.startDate = this.test.startDate
                                    this.newmodel.endDate = this.test.endDate
                                    this.newmodel.progress = this.test.progress
                                    this.newChlindRow[0] = this.newmodel;
                                    this.cruddatamodel[i].subtasks.push(this.newChlindRow[0]);
                                    if(this.cutRows)
                                    {
                                        this.cruddatamodel.splice(this.copiedrowsIndexes[j],1);
                                    }
                                }
                            }

                            // this.cruddatamodel[i].subtasks = [];
                            // this.cruddatamodel[i].subtasks[0] = this.newChlindRow[0];
                        }
                        else if (this.cruddatamodel[i].subtasks.length == 0 || this.cruddatamodel[i].subtasks.length > 0) {
                            // this.newChlindRow = [];
                            // this.newmodel = new datamodal();
                            // this.test = args.rowInfo.rowData;
                            // this.newmodel.taskID = this.test.taskID
                            // this.newmodel.taskName = this.test.taskName
                            // this.newmodel.duration = this.test.duration
                            // this.newmodel.startDate = this.test.startDate
                            // this.newmodel.endDate = this.test.endDate
                            // this.newmodel.progress = this.test.progress
                            // this.newChlindRow[0] = this.newmodel;
                            // this.cruddatamodel[i].subtasks[0] = this.newChlindRow[0];
                            if(this.copiedrowsIndexes.length > 0)
                            {
                                for(var j = 0;this.copiedrowsIndexes.length > j;j++)
                                {
                                        this.newChlindRow = [];
                                        this.newmodel = new datamodal();
                                        this.test = this.cruddatamodel[this.copiedrowsIndexes[j]]
                                        this.newmodel.taskID = this.test.taskID
                                         this.newmodel.taskName = this.test.taskName
                                        this.newmodel.duration = this.test.duration
                                        this.newmodel.startDate = this.test.startDate
                                        this.newmodel.endDate = this.test.endDate
                                        this.newmodel.progress = this.test.progress
                                        this.newChlindRow[0] = this.newmodel;
                                        this.cruddatamodel[i].subtasks.push(this.newChlindRow[0]);
                                        if(this.cutRows)
                                        {
                                            this.cruddatamodel.splice(this.copiedrowsIndexes[i],1);
                                        }
                                }
                            }

                        }
                       // else if (this.cruddatamodel[i].subtasks.length > 0) {
                            // this.newChlindRow = [];
                            // this.newmodel = new datamodal();
                            // this.test = args.rowInfo.rowData;
                            // this.newmodel.taskID = this.test.taskID
                            // this.newmodel.taskName = this.test.taskName
                            // this.newmodel.duration = this.test.duration
                            // this.newmodel.startDate = this.test.startDate
                            // this.newmodel.endDate = this.test.endDate
                            // this.newmodel.progress = this.test.progress
                            // this.newChlindRow[0] = this.newmodel;
                            //this.cruddatamodel[i].subtasks.push(this.newChlindRow[0].taskData);
                        //}
                    }
                }


                // if(this.cruddatamodel[args.rowInfo.rowIndex].subtasks == undefined)
                // {
                //         this.newChlindRow = [];
                //         this.newmodel = new datamodal();
                //         this.test  = args.rowInfo.rowData;
                //         this.newmodel.taskID = this.test.taskID
                //         this.newmodel.taskName = this.test.taskName
                //         this.newmodel.duration = this.test.duration
                //         this.newmodel.startDate = this.test.startDate
                //         this.newmodel.endDate = this.test.endDate
                //         this.newmodel.progress = this.test.progress
                //         this.newChlindRow[0] = this.newmodel;
                //         for(var j = 0;this.cruddatamodel.length > j;j++)
                //         {
                //             if(this.cruddatamodel[i].taskID == this.parentData.parentItem.taskID)
                //             {
                //                 this.cruddatamodel[i].subtasks = [];
                //                 this.cruddatamodel[i].subtasks[0] = this.newChlindRow[0];
                //             }
                //         }
                // }
                // else
                // { 
                //     if(this.cruddatamodel[args.rowInfo.rowIndex].subtasks.length == 0)
                //     {
                //        this.newChlindRow = [];
                //        this.newmodel = new datamodal();
                //        this.test  = args.rowInfo.rowData;
                //        this.newmodel.taskID = this.test.taskID
                //         this.newmodel.taskName = this.test.taskName
                //         this.newmodel.duration = this.test.duration
                //         this.newmodel.startDate = this.test.startDate
                //         this.newmodel.endDate = this.test.endDate
                //         this.newmodel.progress = this.test.progress
                //         this.newChlindRow[0] = this.newmodel;
                //         this.cruddatamodel[args.rowInfo.rowIndex].subtasks[0] = this.newChlindRow[0];
                //     }
                //     else if(this.cruddatamodel[args.rowInfo.rowIndex].subtasks.length > 0)
                //     {
                //         this.newChlindRow[0] = args.rowInfo.rowData;
                //         //this.newChlindRow[0] = Data;
                //         this.cruddatamodel[args.rowInfo.rowIndex].subtasks.push(this.newChlindRow[0].taskData);
                //     }
                // }
            }


            // if(this.lstdatamodal[args.rowInfo.rowIndex].subtasks == undefined)
            // {
            //     if(this.lstdatamodal[args.rowInfo.rowIndex].subtasks.length == 0)
            //     {
            //        this.newChlindRow = [];
            //         this.newChlindRow[0] = args.rowInfo.rowData;
            //         this.lstdatamodal[args.rowInfo.rowIndex].subtasks[0] = this.newChlindRow[0];
            //         this.lstdatamodal[args.rowInfo.rowIndex].subtasks[0].subtasks = [];
            //     }
            // }
            // else
            // { 
            //     if(this.lstdatamodal[args.rowInfo.rowIndex].subtasks.length == 0)
            //     {
            //        this.newChlindRow = [];
            //         this.newChlindRow[0] = args.rowInfo.rowData;
            //         this.lstdatamodal[args.rowInfo.rowIndex].subtasks[0] = this.newChlindRow[0];
            //         this.lstdatamodal[args.rowInfo.rowIndex].subtasks[0].subtasks = [];
            //     }
            //     else if(this.lstdatamodal[args.rowInfo.rowIndex].subtasks.length > 0)
            //     {
            //         this.newChlindRow = [];
            //         this.newChlindRow[0] = args.rowInfo.rowData;
            //         this.lstdatamodal[args.rowInfo.rowIndex].subtasks.push(this.newChlindRow[0]);
            //     }
            // }
            this.saveRecord(this.cruddatamodel);


            // for(var i =0;this.lstdatamodal.length > i;i++)
            // {
            //     if(this.lstdatamodal[i].taskData != undefined)
            //     {
            //         this.lstdatamodal[args.rowInfo.rowIndex].subtasks[0] = this.lstdatamodal[i].taskData
            //         // if(this.lstdatamodal[i].subtasks.length == 1)
            //         // {
            //         //     this.lstdatamodal[i].subtasks[0].subtasks = [];
            //         // }
            //     }

            // }
            //this.saveRecord();
        }
        if(this.StyleColumns || this.NewColumns || this.ShowHideColumns ||
            this.MultiSortColumns)
            {
                    this.gridclass = "col-md-8";
            }
            else
            {
                this.gridclass = "col-md-12";
            }
    }
    Addcolumns() {
        this.Addcol = true;
        this.Editcol = false;
        this.Delcol = false;
    }
    Editcolumns() {
        this.Editcol = true;
        this.Addcol = false;
        this.Delcol = false;
    }
    Delcolumns() {
        this.Delcol = true;
        this.Addcol = false;
        this.Editcol = false;

    }
    InsertColumns() {
        this.objAddTreemodal = new treemodal();
        this.objAddTreemodal.field = this.objTreemodal.headerText.replace(" ", "").trim();
        this.objAddTreemodal.headerText = this.objTreemodal.headerText.trim();
        this.objAddTreemodal.width = "90";
        this.objAddTreemodal.isPrimaryKey = undefined;
        if (this.objAddTreemodal.headerText != "") {
            if (this.lstTreemodal.length > 0) {
                for (let ColName of this.lstTreemodal) {
                    if (ColName.headerText.toLowerCase() == this.objAddTreemodal.headerText.toLowerCase()) {
                        alert("Column already exists.")
                        break;
                    }
                    else {
                        this.lstTreemodal.push(this.objAddTreemodal);
                        for (var i = 0; this.lstdatamodal.length > i; i++) {
                            this.lstdatamodal[i][this.objAddTreemodal.field] = "";
                        }
                        this.saveColumns();
                        this.objAddTreemodal = new treemodal();
                        this.objTreemodal = new treemodal();
                        break;
                    }
                }
            }
            else {
                this.lstTreemodal.push(this.objAddTreemodal);
                for (var i = 0; this.lstdatamodal.length > i; i++) {
                    this.lstdatamodal[i][this.objAddTreemodal.field] = "";
                }
                this.saveColumns();
                this.objAddTreemodal = new treemodal();
                this.objTreemodal = new treemodal();
            }
        }
    }
    updateColumns() {
        this.objAddTreemodal = new treemodal();
        this.objAddTreemodal.field = this.objTreemodal.field;
        this.objAddTreemodal.headerText = this.objTreemodal.headerText;
        if (this.lstTreemodal.length > 0) {
            for (var i = 0; this.lstTreemodal.length > i; i++) {
                if (this.lstTreemodal[i].headerText.toLowerCase() == this.objAddTreemodal.headerText.toLowerCase()) {
                    alert("Column already exists.")
                    break;
                }
                else {
                    if (this.lstTreemodal[i].field == this.objAddTreemodal.field) {
                        this.lstTreemodal[i].headerText = this.objAddTreemodal.headerText;
                        this.saveColumns();
                        break;
                    }
                }
            }
        }
        const column = this.treegrid.getColumnByField(this.objAddTreemodal.field);
        column.headerText = this.objAddTreemodal.headerText;
        this.treegrid.refreshColumns();
    }
    deleteColumns() {
        if (this.lstTreemodal.length > 0) {
            for (var i = 0; this.lstTreemodal.length > i; i++) {
                if (this.lstTreemodal[i].field == this.objTreemodal.field) {
                    this.lstTreemodal.splice(i, 1);
                    for (var i = 0; this.cruddatamodel.length > i; i++) {
                        this.cruddatamodel[i][this.objTreemodal.field] = "";
                    }
                    this.saveColumns();
                    this.saveRecord(this.cruddatamodel);
                    break;
                }
            }



            // for (var i = 0; this.lstTreemodal.length > i; i++) {
            //     if (this.lstTreemodal[i].field == this.objTreemodal.field) {
            //         this.lstTreemodal.splice(i, 1);
            //         for(var i = 0;this.lstdatamodal.length > i;i++){
            //             this.lstdatamodal[i][this.objTreemodal.field] = "";
            //           }
            //         this.saveColumns();
            //         this.saveRecord();
            //         break;
            //     }
            // }
        }
    }
    freezeColumns() {
        for (var i = 0; this.lstTreemodal.length > i; i++) {
            if (this.lstTreemodal[i].field == this.objfreeze.field) {
                this.lstTreemodal[i].freeze = this.objfreeze.direction;
                this.treegrid.grid.getColumnByField(this.lstTreemodal[i].field).freeze =
                    this.objfreeze.direction === 'Center' ? undefined : (this.objfreeze.direction as freezeDirection);
                break;
            }
        }
        this.treegrid.refreshColumns();
    }
    public toolbarClick(args: ClickEventArgs): void {
        switch (args.item.id) {
            case this.treegrid.grid.element.id + '_pdfexport':
                if (this.treegrid.enableRtl === true && this.treegrid.locale === 'ar') {
                    let innercontent: any = 'You need custom fonts to export Arabic characters, refer this'
                        + '<a target="_blank" href="https://ej2.syncfusion.com/angular/documentation/treegrid/pdf-export/#add-custom-font-for-pdf-exporting">'
                        + 'documentation section</a>';
                    DialogUtility.alert({ content: innercontent });
                }
                else {
                    let pdfExportProperties: TreeGridPdfExportProperties = {
                        isCollapsedStatePersist: this.collapseStatePersist
                    };
                    this.treegrid.pdfExport(pdfExportProperties);
                }

                break;
            case this.treegrid.grid.element.id + '_excelexport':
                let excelExportProperties: TreeGridExcelExportProperties = {
                    isCollapsedStatePersist: this.collapseStatePersist
                };
                this.treegrid.excelExport(excelExportProperties);
                break;
            case this.treegrid.grid.element.id + '_csvexport':
                this.treegrid.csvExport();
                break;

        }
        if(args.item.id == "copy")
        {
            if (this.treegrid.getSelectedRecords().length > 0) {
                this.cutRows = false;
                this.copiedrowsIndexes = [];
                this.copiedrowsIndexes = this.treegrid.getSelectedRowIndexes();
            } else {
                this.alertDialog.show();
            }
        }
        if(args.item.id == "cut")
        {
            if (this.treegrid.getSelectedRecords().length > 0) {
                this.cutRows = true;
                this.copiedrowsIndexes = [];
                this.copiedrowsIndexes = this.treegrid.getSelectedRowIndexes();
            } else {
                this.alertDialog.show();
            }
        }

    }

    columnHeaderSelected(args: TreeGridColumn): void {
        this.selectedColumn = args;
        this.selectedColumnHeader = args.field;
        let styliesAlreadyApplied = this.stylesApplied.filter(x => x.columnField == args.field);
        if (styliesAlreadyApplied != undefined && styliesAlreadyApplied.length > 0) {
            this.defaultDatatypeSelected = styliesAlreadyApplied[0].dataType;
            this.defaultcellFontSize = styliesAlreadyApplied[0].fontSize.replace("px", "");
            this.defaultcellColor = styliesAlreadyApplied[0].fontColor;
            this.defaultcellBgColor = styliesAlreadyApplied[0].bgColor;
            this.defaultAlignment = styliesAlreadyApplied[0].alignment;
            this.defaultTextWrap = styliesAlreadyApplied[0].wrap;
            this.minWidthofSelectedColumn = styliesAlreadyApplied[0].minimumWidth;

        } else {
            this.minWidthofSelectedColumn = <number>args.width;
            this.defaultDatatypeSelected = args.editType == "" ? "defaultedit" : args.editType.toString();
            this.defaultAlignment = args.textAlign.toString() == "" ? 'Center' : args.textAlign.toString();
            this.defaultcellFontSize = '12';
            this.defaultcellColor = '#000000';
            this.defaultcellBgColor = '#FFFFFF';
            this.defaultTextWrap = 'both';

        }
    }
    addStyles(): void {


        let columnToOperate = this.treegrid.getColumnByUid(this.selectedColumn.uid);

        columnToOperate.editType = this.dropdown2.value.toString();
        columnToOperate.textAlign = this.dropdown3.value.toString() == "Center" ? 'Center' :
            this.dropdown3.value.toString() == 'Left' ? 'Left' : this.dropdown3.value.toString() == 'Justify' ? 'Justify' :
                this.dropdown3.value.toString() == 'Right' ? 'Right' : 'Center';
        columnToOperate.minWidth = this.minimumWidthInput.value;
        let columnDOM = document.querySelectorAll('[e-mappinguid="' + this.selectedColumn.uid + '"]')[0] as HTMLDivElement
        columnDOM.style.fontSize = this.fontsize.value + "px";
        let stylesAlreadyApplied = this.stylesApplied.filter(x => x.columnField == columnToOperate.field);
        let parentElement = <HTMLParentExtension>columnDOM.parentElement
        if (stylesAlreadyApplied.length > 0) {

            stylesAlreadyApplied[0].alignment = columnToOperate.textAlign;
            stylesAlreadyApplied[0].bgColor = this.BgColor.value;
            stylesAlreadyApplied[0].fontColor = this.fontColor.value;
            stylesAlreadyApplied[0].cellIndex = parentElement.cellIndex;
            stylesAlreadyApplied[0].fontSize = this.fontsize.value + "px";
            stylesAlreadyApplied[0].dataType = this.dropdown2.value.toString();
            stylesAlreadyApplied[0].minimumWidth = this.minimumWidthInput.value;
            stylesAlreadyApplied[0].refreshColumn = false;
            stylesAlreadyApplied[0].UiD = this.selectedColumn.uid;

        } else {
            this.stylesApplied.push(<Styles>{
                alignment: columnToOperate.textAlign,
                bgColor: this.BgColor.value,
                fontSize: this.fontsize.value + "px",
                cellIndex: parentElement.cellIndex,
                fontColor: this.fontColor.value,
                columnField: columnToOperate.field,
                dataType: this.dropdown2.value.toString(),
                minimumWidth: this.minimumWidthInput.value,
                refreshColumn: false,
                UiD: this.selectedColumn.uid
            });
        }
    }
    applyStyleChanges(): void {
        for (let i = 0; i < this.stylesApplied.length; i++) {

            this.applyChanges(this.stylesApplied[i]);
        }
    }
    applyStyleChangesToDOM(): void {
        for (let i = 0; i < this.stylesApplied.length; i++) {

            this.applychangestoHeader(this.stylesApplied[i]);
            this.applyChangestoTableBody(this.stylesApplied[i]);
        }
    }
    applyChanges(args: Styles): void {
        let columnToOperate = this.treegrid.getColumnByUid(args.UiD);


        columnToOperate.editType = args.dataType;
        columnToOperate.textAlign = args.alignment == "Center" ? 'Center' :
            args.alignment == 'Left' ? 'Left' : args.alignment == 'Justify' ? 'Justify' :
                args.alignment == 'Right' ? 'Right' : 'Center';
        columnToOperate.minWidth = args.minimumWidth;

        this.treegrid.refreshColumns();






    }
    applychangestoHeader(args: Styles): void {
        let columnDOM = document.querySelectorAll('[e-mappinguid="' + args.UiD + '"]')[0] as HTMLDivElement
        let cssElement = <HTMLCollectionOf<ElementExtender>>columnDOM.getElementsByClassName('e-headertext');
        cssElement[0].style.color = args.fontColor;
        cssElement[0].style.fontSize = args.fontSize;
        columnDOM.parentElement.style.backgroundColor = args.bgColor;
        columnDOM.parentElement.style.backgroundColor = args.bgColor;
        cssElement[0].style.color = args.fontColor;
        cssElement[0].style.color = args.fontColor;
    }
    applyChangestoTableBody(args: Styles): void {
        let allrows = document.getElementsByClassName('e-gridcontent')[0].
            getElementsByTagName('table')[0].getElementsByTagName('tbody')[0].
            getElementsByTagName('tr');

        for (let i = 0; i < allrows.length; i++) {
            if (allrows[i].getElementsByTagName('td').length > 0) {
                allrows[i].getElementsByTagName('td')[args.cellIndex].style.fontSize = args.fontSize;

                allrows[i].getElementsByTagName('td')[args.cellIndex].style.background = args.bgColor;

                allrows[i].getElementsByTagName('td')[args.cellIndex].style.color = args.fontColor;
            }
        }
    }
    changeColumnType(args?: ChangeEventArgs): void {
        this.addStyles();
        let stylingColumn = this.stylesApplied.filter(x => x.columnField == this.selectedColumn.field);
        if (stylingColumn.length > 0) {
            stylingColumn[0].refreshColumn = true;
        }
        /* this.applyStyleChanges();*/
    }
    minWidthfocusOut(args?: Object): void {

        //this.addStyles();
        //this.applyStyleChanges();
    }
    minWidthfocusIn(args?: Object): void {
        //this.addStyles();
        //this.applyStyleChanges();
    }
    changeFontSize(args?: Object): void {

        //this.addStyles();
        //this.applyStyleChanges();
    }
    changefontColor(args?: ColorPickerEventArgs): void {
        this.fontColor.value = args.currentValue.hex;
        //columnDOM.getElementsByClassName('e-headertext')[0].style.color = args.currentValue.hex;
        //this.applyChangestoTableBody(columnDOM.parentElement.cellIndex,
        //    this.BgColor.value, args.currentValue.hex, true, false);
        //this.addStyles();
        //this.applyStyleChanges();

    }
    changeBgColor(args?: ColorPickerEventArgs): void {
        this.BgColor.value = args.currentValue.hex;
        //this.addStyles();
        //this.applyStyleChanges();
        //this.applyChanges(true, false);
        //this.applyChangestoTableBody(columnDOM.parentElement.cellIndex,
        //    args.currentValue.hex, this.fontColor.value, false, true);
        //columnDOM.style.backgroundColor = args.currentValue.hex;

    }
    changeAlignment(args?: Object): void {
        this.addStyles();
        let stylingColumn = this.stylesApplied.filter(x => x.columnField == this.selectedColumn.field);
        if (stylingColumn.length > 0) {
            stylingColumn[0].refreshColumn = true;
        }

        /*this.applyChanges(true, true, true);*/
        //let columnDOM = document.querySelectorAll('[e-mappinguid="' + this.selectedColumn.uid + '"]')[0] as HTMLDivElement


        //this.applyChangestoTableBody(columnDOM.parentElement.cellIndex,
        //    this.BgColor.value, this.fontColor.value, true, true);
    }
    applyChangesAndSave(): void {
        this.addStyles();
        this.applyStyleChanges();
        //this.applyStyleChangesToDOM();
        this.saveColumnStyles();
    }
    columnHeaderChange(args?: ChangeEventArgs): void {
        this.selectedColumn = <TreeGridColumn>this.treegrid.getColumnByField(args.value.toString());
        this.columnHeaderSelected(this.selectedColumn);

    }
    changeWrapSettings(args?: ChangeEventArgs): void {
        this.addStyles();
        this.applyStyleChanges();
    }

    rowSelected(args: RowSelectEventArgs) {
        this.trRow = args.row as HTMLTableRowElement;
        this.rowDetails = args.row;
    }
    getRecord() {

        this._svc.getData('Data/readData').subscribe(
            data => {
                this.lstdatamodal = JSON.parse(data);
                this.cruddatamodel = this.lstdatamodal

            }, (err) => {

                console.log(err)

            });
    }
    saveRecord(data: datamodal[]) {

        //    for(var i =0;this.lstdatamodal.length > i;i++)
        //     {
        //         if(this.lstdatamodal[i].subtasks != undefined)
        //         {
        //             if(this.lstdatamodal[i].subtasks.length == 1)
        //             {
        //                 this.lstdatamodal[i].subtasks[0].subtasks = [];
        //             }
        //         }

        //     }
        console.log(this.lstdatamodal)
        this._svc.saveData(data, 'Data/postData').subscribe(
            data => {

                this.getRecord();

            }, (err) => {

                console.log(err)

            });
    }
    getColumns() {

        this._svc.getColumns('Data/readColumns').subscribe(
            data => {
                this.lstTreemodal = JSON.parse(data);
                for (var i = 0; this.lstTreemodal.length > i; i++) {
                    this.d3data.push(this.lstTreemodal[i].field)
                }
                if (this.lstTreemodal.length == 0) {
                    this.Addcol = true
                    this.NewColumns = true;
                }
                else {
                    this.Addcol = false
                    this.NewColumns = false;
                }
            }, (err) => {

                console.log(err)

            });
    }
    saveColumns() {

        this._svc.saveColumns(this.lstTreemodal, 'Data/postColumns').subscribe(
            data => {
            }, (err) => {
                console.log(err)
            });
    }
    getColumnStyles() {
        this._svc.getColumns('Data/readStyles').subscribe(data => {
            if (data != "") {
                this.stylesApplied = JSON.parse(data);
                this.applyStyleChanges();
                //if (this.stylesApplied.length > 0) {
                //    for (let i = 0; i < this.stylesApplied.length; i++) {

                //        this.applyChanges(this.stylesApplied[i]);
                //    }
                //}
            } else {
                this.stylesApplied = [];
            }

        }, (err) => {

            console.log(err)

        });
    }
    saveColumnStyles() {
        this._svc.saveStyles(this.stylesApplied, 'Data/saveColumnStyles').subscribe(
            data => {
                //this.lstdatamodal = JSON.parse(data);

            }, (err) => {

                console.log(err)

            });
    }
    Cancel(){

        this.NewColumns = false;
        this.ShowHideColumns = false;
        this.FreezeColumns = false;
        this.FilterColumns = false;
        this.MultiSortColumns = false;
        this.StyleColumns = false;
        this.gridclass = "col-md-12";
    }
}

interface ContextMenuArgs extends MenuEventArgs {
    column: TreeGridColumn,
    rowInfo: RowInfo;
}
interface Styles {
    cellIndex: number,
    bgColor: string,
    fontColor: string,
    alignment: string,
    fontSize: string,
    columnField: string,
    dataType: string,
    minimumWidth: number,
    refreshColumn: boolean,
    UiD: string,
    wrap: string

}
interface HTMLParentExtension extends HTMLElement {
    cellIndex: number;
}
interface ElementExtender extends Element {
    style: CSSStyleDeclaration
}




