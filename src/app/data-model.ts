export class datamodal {
    taskID: any
    taskName: any
    startDate: Date
    endDate: Date
    duration: any
    progress: any
    subtasks : subdatamodel[]
    taskData: any
}
export class subdatamodel {
    taskID: any
    taskName: any
    startDate: Date
    endDate: Date
    duration: any
    progress: any
    subtasks : any
}