import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import { ColumnChooserService, ExcelExportService, InfiniteScrollService, PdfExportService, TreeGridModule } from "@syncfusion/ej2-angular-treegrid";
import { NumericTextBoxModule, ColorPickerModule } from '@syncfusion/ej2-angular-inputs';
import {  DropDownListModule } from "@syncfusion/ej2-angular-dropdowns";
import {
  PageService,
  SortService,
  EditService,
  FilterService,
  ToolbarService,
  ResizeService,
  ReorderService,
  RowDDService,
  FreezeService,
  ContextMenuService 
} from "@syncfusion/ej2-angular-treegrid";
import {  ButtonModule,  CheckBoxModule } from "@syncfusion/ej2-angular-buttons";
import {  DialogModule } from "@syncfusion/ej2-angular-popups";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [AppComponent],
    imports: [BrowserModule, TreeGridModule, FormsModule, DropDownListModule, CheckBoxModule, ButtonModule, DialogModule, NumericTextBoxModule, ColorPickerModule,HttpClientModule,],
  providers: [PageService, SortService,EditService,ToolbarService,ResizeService,ReorderService,RowDDService,FilterService,ContextMenuService,FreezeService,ExcelExportService, PdfExportService,ColumnChooserService,InfiniteScrollService],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
