export let sampleData: any[] = [
  {
    "taskID": 0,
    "taskName": "Aguirre",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 0,
    "progress": 0
  },
  {
    "taskID": 1,
    "taskName": "Carla",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 1,
    "progress": 1
  },
  {
    "taskID": 2,
    "taskName": "Gayle",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 2,
    "progress": 2
  },
  {
    "taskID": 3,
    "taskName": "Odonnell",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 3,
    "progress": 3
  },
  {
    "taskID": 4,
    "taskName": "Bass",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 4,
    "progress": 4
  },
  {
    "taskID": 5,
    "taskName": "Antonia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 5,
    "progress": 5
  },
  {
    "taskID": 6,
    "taskName": "Albert",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 6,
    "progress": 6
  },
  {
    "taskID": 7,
    "taskName": "Janet",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 7,
    "progress": 7
  },
  {
    "taskID": 8,
    "taskName": "Martina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 8,
    "progress": 8
  },
  {
    "taskID": 9,
    "taskName": "Annette",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 9,
    "progress": 9
  },
  {
    "taskID": 10,
    "taskName": "Molly",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 10,
    "progress": 10
  },
  {
    "taskID": 11,
    "taskName": "Kristie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 11,
    "progress": 11
  },
  {
    "taskID": 12,
    "taskName": "Bell",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 12,
    "progress": 12
  },
  {
    "taskID": 13,
    "taskName": "Jennie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 13,
    "progress": 13
  },
  {
    "taskID": 14,
    "taskName": "Gretchen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 14,
    "progress": 14
  },
  {
    "taskID": 15,
    "taskName": "Meagan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 15,
    "progress": 15
  },
  {
    "taskID": 16,
    "taskName": "Samantha",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 16,
    "progress": 16
  },
  {
    "taskID": 17,
    "taskName": "Mattie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 17,
    "progress": 17
  },
  {
    "taskID": 18,
    "taskName": "Kristine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 18,
    "progress": 18
  },
  {
    "taskID": 19,
    "taskName": "Bessie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 19,
    "progress": 19
  },
  {
    "taskID": 20,
    "taskName": "Shirley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 20,
    "progress": 20
  },
  {
    "taskID": 21,
    "taskName": "Boyle",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 21,
    "progress": 21
  },
  {
    "taskID": 22,
    "taskName": "Roxanne",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 22,
    "progress": 22
  },
  {
    "taskID": 23,
    "taskName": "Jenifer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 23,
    "progress": 23
  },
  {
    "taskID": 24,
    "taskName": "Cochran",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 24,
    "progress": 24
  },
  {
    "taskID": 25,
    "taskName": "Fern",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 25,
    "progress": 25
  },
  {
    "taskID": 26,
    "taskName": "Chelsea",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 26,
    "progress": 26
  },
  {
    "taskID": 27,
    "taskName": "Head",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 27,
    "progress": 27
  },
  {
    "taskID": 28,
    "taskName": "Miller",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 28,
    "progress": 28
  },
  {
    "taskID": 29,
    "taskName": "Leblanc",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 29,
    "progress": 29
  },
  {
    "taskID": 30,
    "taskName": "Berta",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 30,
    "progress": 30
  },
  {
    "taskID": 31,
    "taskName": "Kaitlin",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 31,
    "progress": 31
  },
  {
    "taskID": 32,
    "taskName": "Lakeisha",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 32,
    "progress": 32
  },
  {
    "taskID": 33,
    "taskName": "Edna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 33,
    "progress": 33
  },
  {
    "taskID": 34,
    "taskName": "Patty",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 34,
    "progress": 34
  },
  {
    "taskID": 35,
    "taskName": "Henry",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 35,
    "progress": 35
  },
  {
    "taskID": 36,
    "taskName": "Pruitt",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 36,
    "progress": 36
  },
  {
    "taskID": 37,
    "taskName": "Holder",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 37,
    "progress": 37
  },
  {
    "taskID": 38,
    "taskName": "Alice",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 38,
    "progress": 38
  },
  {
    "taskID": 39,
    "taskName": "Bolton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 39,
    "progress": 39
  },
  {
    "taskID": 40,
    "taskName": "Ora",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 40,
    "progress": 40
  },
  {
    "taskID": 41,
    "taskName": "Laurel",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 41,
    "progress": 41
  },
  {
    "taskID": 42,
    "taskName": "Margery",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 42,
    "progress": 42
  },
  {
    "taskID": 43,
    "taskName": "Pollard",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 43,
    "progress": 43
  },
  {
    "taskID": 44,
    "taskName": "English",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 44,
    "progress": 44
  },
  {
    "taskID": 45,
    "taskName": "Tessa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 45,
    "progress": 45
  },
  {
    "taskID": 46,
    "taskName": "Lucile",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 46,
    "progress": 46
  },
  {
    "taskID": 47,
    "taskName": "Beverly",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 47,
    "progress": 47
  },
  {
    "taskID": 48,
    "taskName": "Barnett",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 48,
    "progress": 48
  },
  {
    "taskID": 49,
    "taskName": "Roman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 49,
    "progress": 49
  },
  {
    "taskID": 50,
    "taskName": "Walsh",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 50,
    "progress": 50
  },
  {
    "taskID": 51,
    "taskName": "Jewel",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 51,
    "progress": 51
  },
  {
    "taskID": 52,
    "taskName": "Noble",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 52,
    "progress": 52
  },
  {
    "taskID": 53,
    "taskName": "York",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 53,
    "progress": 53
  },
  {
    "taskID": 54,
    "taskName": "Wolfe",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 54,
    "progress": 54
  },
  {
    "taskID": 55,
    "taskName": "Robert",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 55,
    "progress": 55
  },
  {
    "taskID": 56,
    "taskName": "Bettye",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 56,
    "progress": 56
  },
  {
    "taskID": 57,
    "taskName": "Leanna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 57,
    "progress": 57
  },
  {
    "taskID": 58,
    "taskName": "Kathryn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 58,
    "progress": 58
  },
  {
    "taskID": 59,
    "taskName": "Rollins",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 59,
    "progress": 59
  },
  {
    "taskID": 60,
    "taskName": "Leigh",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 60,
    "progress": 60
  },
  {
    "taskID": 61,
    "taskName": "Lolita",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 61,
    "progress": 61
  },
  {
    "taskID": 62,
    "taskName": "Christine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 62,
    "progress": 62
  },
  {
    "taskID": 63,
    "taskName": "Latoya",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 63,
    "progress": 63
  },
  {
    "taskID": 64,
    "taskName": "Wilda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 64,
    "progress": 64
  },
  {
    "taskID": 65,
    "taskName": "Day",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 65,
    "progress": 65
  },
  {
    "taskID": 66,
    "taskName": "Sandy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 66,
    "progress": 66
  },
  {
    "taskID": 67,
    "taskName": "Sophia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 67,
    "progress": 67
  },
  {
    "taskID": 68,
    "taskName": "Vera",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 68,
    "progress": 68
  },
  {
    "taskID": 69,
    "taskName": "Miranda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 69,
    "progress": 69
  },
  {
    "taskID": 70,
    "taskName": "Fletcher",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 70,
    "progress": 70
  },
  {
    "taskID": 71,
    "taskName": "Irene",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 71,
    "progress": 71
  },
  {
    "taskID": 72,
    "taskName": "Carver",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 72,
    "progress": 72
  },
  {
    "taskID": 73,
    "taskName": "Mckinney",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 73,
    "progress": 73
  },
  {
    "taskID": 74,
    "taskName": "Rhodes",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 74,
    "progress": 74
  },
  {
    "taskID": 75,
    "taskName": "Nelda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 75,
    "progress": 75
  },
  {
    "taskID": 76,
    "taskName": "Candace",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 76,
    "progress": 76
  },
  {
    "taskID": 77,
    "taskName": "Matilda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 77,
    "progress": 77
  },
  {
    "taskID": 78,
    "taskName": "Ross",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 78,
    "progress": 78
  },
  {
    "taskID": 79,
    "taskName": "Sloan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 79,
    "progress": 79
  },
  {
    "taskID": 80,
    "taskName": "Susie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 80,
    "progress": 80
  },
  {
    "taskID": 81,
    "taskName": "Jo",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 81,
    "progress": 81
  },
  {
    "taskID": 82,
    "taskName": "Palmer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 82,
    "progress": 82
  },
  {
    "taskID": 83,
    "taskName": "Rodriquez",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 83,
    "progress": 83
  },
  {
    "taskID": 84,
    "taskName": "Grimes",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 84,
    "progress": 84
  },
  {
    "taskID": 85,
    "taskName": "Ruthie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 85,
    "progress": 85
  },
  {
    "taskID": 86,
    "taskName": "Maggie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 86,
    "progress": 86
  },
  {
    "taskID": 87,
    "taskName": "Jami",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 87,
    "progress": 87
  },
  {
    "taskID": 88,
    "taskName": "Antoinette",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 88,
    "progress": 88
  },
  {
    "taskID": 89,
    "taskName": "Myra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 89,
    "progress": 89
  },
  {
    "taskID": 90,
    "taskName": "Wyatt",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 90,
    "progress": 90
  },
  {
    "taskID": 91,
    "taskName": "Hopper",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 91,
    "progress": 91
  },
  {
    "taskID": 92,
    "taskName": "Lily",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 92,
    "progress": 92
  },
  {
    "taskID": 93,
    "taskName": "Evangeline",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 93,
    "progress": 93
  },
  {
    "taskID": 94,
    "taskName": "Allison",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 94,
    "progress": 94
  },
  {
    "taskID": 95,
    "taskName": "Oliver",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 95,
    "progress": 95
  },
  {
    "taskID": 96,
    "taskName": "Barry",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 96,
    "progress": 96
  },
  {
    "taskID": 97,
    "taskName": "Edwards",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 97,
    "progress": 97
  },
  {
    "taskID": 98,
    "taskName": "Tracey",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 98,
    "progress": 98
  },
  {
    "taskID": 99,
    "taskName": "Lauri",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 99,
    "progress": 99
  },
  {
    "taskID": 100,
    "taskName": "Jeanie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 100,
    "progress": 100
  },
  {
    "taskID": 101,
    "taskName": "Joyner",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 101,
    "progress": 101
  },
  {
    "taskID": 102,
    "taskName": "Bond",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 102,
    "progress": 102
  },
  {
    "taskID": 103,
    "taskName": "Kitty",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 103,
    "progress": 103
  },
  {
    "taskID": 104,
    "taskName": "Ruth",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 104,
    "progress": 104
  },
  {
    "taskID": 105,
    "taskName": "Penelope",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 105,
    "progress": 105
  },
  {
    "taskID": 106,
    "taskName": "Traci",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 106,
    "progress": 106
  },
  {
    "taskID": 107,
    "taskName": "Carolina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 107,
    "progress": 107
  },
  {
    "taskID": 108,
    "taskName": "Cunningham",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 108,
    "progress": 108
  },
  {
    "taskID": 109,
    "taskName": "Louise",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 109,
    "progress": 109
  },
  {
    "taskID": 110,
    "taskName": "Chrystal",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 110,
    "progress": 110
  },
  {
    "taskID": 111,
    "taskName": "Lopez",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 111,
    "progress": 111
  },
  {
    "taskID": 112,
    "taskName": "Celina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 112,
    "progress": 112
  },
  {
    "taskID": 113,
    "taskName": "Flores",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 113,
    "progress": 113
  },
  {
    "taskID": 114,
    "taskName": "Elvia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 114,
    "progress": 114
  },
  {
    "taskID": 115,
    "taskName": "Katy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 115,
    "progress": 115
  },
  {
    "taskID": 116,
    "taskName": "Dianne",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 116,
    "progress": 116
  },
  {
    "taskID": 117,
    "taskName": "Gwen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 117,
    "progress": 117
  },
  {
    "taskID": 118,
    "taskName": "Cook",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 118,
    "progress": 118
  },
  {
    "taskID": 119,
    "taskName": "Deanna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 119,
    "progress": 119
  },
  {
    "taskID": 120,
    "taskName": "Edwina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 120,
    "progress": 120
  },
  {
    "taskID": 121,
    "taskName": "Rosa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 121,
    "progress": 121
  },
  {
    "taskID": 122,
    "taskName": "Dorothea",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 122,
    "progress": 122
  },
  {
    "taskID": 123,
    "taskName": "Salas",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 123,
    "progress": 123
  },
  {
    "taskID": 124,
    "taskName": "Sherman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 124,
    "progress": 124
  },
  {
    "taskID": 125,
    "taskName": "Audra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 125,
    "progress": 125
  },
  {
    "taskID": 126,
    "taskName": "Briana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 126,
    "progress": 126
  },
  {
    "taskID": 127,
    "taskName": "Jeannine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 127,
    "progress": 127
  },
  {
    "taskID": 128,
    "taskName": "Pam",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 128,
    "progress": 128
  },
  {
    "taskID": 129,
    "taskName": "Letitia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 129,
    "progress": 129
  },
  {
    "taskID": 130,
    "taskName": "Barber",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 130,
    "progress": 130
  },
  {
    "taskID": 131,
    "taskName": "Burton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 131,
    "progress": 131
  },
  {
    "taskID": 132,
    "taskName": "Beach",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 132,
    "progress": 132
  },
  {
    "taskID": 133,
    "taskName": "Hobbs",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 133,
    "progress": 133
  },
  {
    "taskID": 134,
    "taskName": "Lee",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 134,
    "progress": 134
  },
  {
    "taskID": 135,
    "taskName": "Liliana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 135,
    "progress": 135
  },
  {
    "taskID": 136,
    "taskName": "Pena",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 136,
    "progress": 136
  },
  {
    "taskID": 137,
    "taskName": "Hurst",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 137,
    "progress": 137
  },
  {
    "taskID": 138,
    "taskName": "Regina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 138,
    "progress": 138
  },
  {
    "taskID": 139,
    "taskName": "Villarreal",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 139,
    "progress": 139
  },
  {
    "taskID": 140,
    "taskName": "Tammie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 140,
    "progress": 140
  },
  {
    "taskID": 141,
    "taskName": "Phyllis",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 141,
    "progress": 141
  },
  {
    "taskID": 142,
    "taskName": "Kris",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 142,
    "progress": 142
  },
  {
    "taskID": 143,
    "taskName": "Harrington",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 143,
    "progress": 143
  },
  {
    "taskID": 144,
    "taskName": "Jean",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 144,
    "progress": 144
  },
  {
    "taskID": 145,
    "taskName": "Jacobs",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 145,
    "progress": 145
  },
  {
    "taskID": 146,
    "taskName": "Tracie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 146,
    "progress": 146
  },
  {
    "taskID": 147,
    "taskName": "Joseph",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 147,
    "progress": 147
  },
  {
    "taskID": 148,
    "taskName": "Swanson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 148,
    "progress": 148
  },
  {
    "taskID": 149,
    "taskName": "Webb",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 149,
    "progress": 149
  },
  {
    "taskID": 150,
    "taskName": "Lawanda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 150,
    "progress": 150
  },
  {
    "taskID": 151,
    "taskName": "Soto",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 151,
    "progress": 151
  },
  {
    "taskID": 152,
    "taskName": "Socorro",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 152,
    "progress": 152
  },
  {
    "taskID": 153,
    "taskName": "Atkins",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 153,
    "progress": 153
  },
  {
    "taskID": 154,
    "taskName": "Ingram",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 154,
    "progress": 154
  },
  {
    "taskID": 155,
    "taskName": "Olive",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 155,
    "progress": 155
  },
  {
    "taskID": 156,
    "taskName": "Ursula",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 156,
    "progress": 156
  },
  {
    "taskID": 157,
    "taskName": "Whitney",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 157,
    "progress": 157
  },
  {
    "taskID": 158,
    "taskName": "Mendoza",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 158,
    "progress": 158
  },
  {
    "taskID": 159,
    "taskName": "Navarro",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 159,
    "progress": 159
  },
  {
    "taskID": 160,
    "taskName": "Lupe",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 160,
    "progress": 160
  },
  {
    "taskID": 161,
    "taskName": "Maricela",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 161,
    "progress": 161
  },
  {
    "taskID": 162,
    "taskName": "Poole",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 162,
    "progress": 162
  },
  {
    "taskID": 163,
    "taskName": "Maria",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 163,
    "progress": 163
  },
  {
    "taskID": 164,
    "taskName": "Holland",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 164,
    "progress": 164
  },
  {
    "taskID": 165,
    "taskName": "Dorsey",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 165,
    "progress": 165
  },
  {
    "taskID": 166,
    "taskName": "Brady",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 166,
    "progress": 166
  },
  {
    "taskID": 167,
    "taskName": "Wilma",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 167,
    "progress": 167
  },
  {
    "taskID": 168,
    "taskName": "Rosie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 168,
    "progress": 168
  },
  {
    "taskID": 169,
    "taskName": "Magdalena",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 169,
    "progress": 169
  },
  {
    "taskID": 170,
    "taskName": "Bridget",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 170,
    "progress": 170
  },
  {
    "taskID": 171,
    "taskName": "Vonda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 171,
    "progress": 171
  },
  {
    "taskID": 172,
    "taskName": "Browning",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 172,
    "progress": 172
  },
  {
    "taskID": 173,
    "taskName": "Shana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 173,
    "progress": 173
  },
  {
    "taskID": 174,
    "taskName": "Luna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 174,
    "progress": 174
  },
  {
    "taskID": 175,
    "taskName": "Linda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 175,
    "progress": 175
  },
  {
    "taskID": 176,
    "taskName": "Kelley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 176,
    "progress": 176
  },
  {
    "taskID": 177,
    "taskName": "Beck",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 177,
    "progress": 177
  },
  {
    "taskID": 178,
    "taskName": "House",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 178,
    "progress": 178
  },
  {
    "taskID": 179,
    "taskName": "Huber",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 179,
    "progress": 179
  },
  {
    "taskID": 180,
    "taskName": "Norman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 180,
    "progress": 180
  },
  {
    "taskID": 181,
    "taskName": "Brennan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 181,
    "progress": 181
  },
  {
    "taskID": 182,
    "taskName": "Rich",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 182,
    "progress": 182
  },
  {
    "taskID": 183,
    "taskName": "Naomi",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 183,
    "progress": 183
  },
  {
    "taskID": 184,
    "taskName": "Addie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 184,
    "progress": 184
  },
  {
    "taskID": 185,
    "taskName": "Foreman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 185,
    "progress": 185
  },
  {
    "taskID": 186,
    "taskName": "Alana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 186,
    "progress": 186
  },
  {
    "taskID": 187,
    "taskName": "Drake",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 187,
    "progress": 187
  },
  {
    "taskID": 188,
    "taskName": "Gina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 188,
    "progress": 188
  },
  {
    "taskID": 189,
    "taskName": "Carmella",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 189,
    "progress": 189
  },
  {
    "taskID": 190,
    "taskName": "Roach",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 190,
    "progress": 190
  },
  {
    "taskID": 191,
    "taskName": "Lenore",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 191,
    "progress": 191
  },
  {
    "taskID": 192,
    "taskName": "Joni",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 192,
    "progress": 192
  },
  {
    "taskID": 193,
    "taskName": "Kathy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 193,
    "progress": 193
  },
  {
    "taskID": 194,
    "taskName": "Simon",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 194,
    "progress": 194
  },
  {
    "taskID": 195,
    "taskName": "Sheila",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 195,
    "progress": 195
  },
  {
    "taskID": 196,
    "taskName": "Karen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 196,
    "progress": 196
  },
  {
    "taskID": 197,
    "taskName": "Dickson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 197,
    "progress": 197
  },
  {
    "taskID": 198,
    "taskName": "Deena",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 198,
    "progress": 198
  },
  {
    "taskID": 199,
    "taskName": "Woodard",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 199,
    "progress": 199
  },
  {
    "taskID": 200,
    "taskName": "Bradshaw",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 200,
    "progress": 200
  },
  {
    "taskID": 201,
    "taskName": "Rachelle",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 201,
    "progress": 201
  },
  {
    "taskID": 202,
    "taskName": "Durham",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 202,
    "progress": 202
  },
  {
    "taskID": 203,
    "taskName": "Whitley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 203,
    "progress": 203
  },
  {
    "taskID": 204,
    "taskName": "Herminia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 204,
    "progress": 204
  },
  {
    "taskID": 205,
    "taskName": "Bernadette",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 205,
    "progress": 205
  },
  {
    "taskID": 206,
    "taskName": "Bean",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 206,
    "progress": 206
  },
  {
    "taskID": 207,
    "taskName": "Marva",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 207,
    "progress": 207
  },
  {
    "taskID": 208,
    "taskName": "Erica",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 208,
    "progress": 208
  },
  {
    "taskID": 209,
    "taskName": "Erma",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 209,
    "progress": 209
  },
  {
    "taskID": 210,
    "taskName": "Kerr",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 210,
    "progress": 210
  },
  {
    "taskID": 211,
    "taskName": "Patrica",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 211,
    "progress": 211
  },
  {
    "taskID": 212,
    "taskName": "Lorna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 212,
    "progress": 212
  },
  {
    "taskID": 213,
    "taskName": "Joy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 213,
    "progress": 213
  },
  {
    "taskID": 214,
    "taskName": "Ivy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 214,
    "progress": 214
  },
  {
    "taskID": 215,
    "taskName": "Mitchell",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 215,
    "progress": 215
  },
  {
    "taskID": 216,
    "taskName": "Aurora",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 216,
    "progress": 216
  },
  {
    "taskID": 217,
    "taskName": "Knight",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 217,
    "progress": 217
  },
  {
    "taskID": 218,
    "taskName": "Mercer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 218,
    "progress": 218
  },
  {
    "taskID": 219,
    "taskName": "Faith",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 219,
    "progress": 219
  },
  {
    "taskID": 220,
    "taskName": "Curtis",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 220,
    "progress": 220
  },
  {
    "taskID": 221,
    "taskName": "Patel",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 221,
    "progress": 221
  },
  {
    "taskID": 222,
    "taskName": "Maldonado",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 222,
    "progress": 222
  },
  {
    "taskID": 223,
    "taskName": "Elisabeth",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 223,
    "progress": 223
  },
  {
    "taskID": 224,
    "taskName": "Andrews",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 224,
    "progress": 224
  },
  {
    "taskID": 225,
    "taskName": "Callahan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 225,
    "progress": 225
  },
  {
    "taskID": 226,
    "taskName": "Lane",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 226,
    "progress": 226
  },
  {
    "taskID": 227,
    "taskName": "Bridgette",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 227,
    "progress": 227
  },
  {
    "taskID": 228,
    "taskName": "Manuela",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 228,
    "progress": 228
  },
  {
    "taskID": 229,
    "taskName": "Williamson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 229,
    "progress": 229
  },
  {
    "taskID": 230,
    "taskName": "Alisa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 230,
    "progress": 230
  },
  {
    "taskID": 231,
    "taskName": "Fowler",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 231,
    "progress": 231
  },
  {
    "taskID": 232,
    "taskName": "Moody",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 232,
    "progress": 232
  },
  {
    "taskID": 233,
    "taskName": "Lorena",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 233,
    "progress": 233
  },
  {
    "taskID": 234,
    "taskName": "Terry",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 234,
    "progress": 234
  },
  {
    "taskID": 235,
    "taskName": "Price",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 235,
    "progress": 235
  },
  {
    "taskID": 236,
    "taskName": "Carole",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 236,
    "progress": 236
  },
  {
    "taskID": 237,
    "taskName": "Parks",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 237,
    "progress": 237
  },
  {
    "taskID": 238,
    "taskName": "Holman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 238,
    "progress": 238
  },
  {
    "taskID": 239,
    "taskName": "Graves",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 239,
    "progress": 239
  },
  {
    "taskID": 240,
    "taskName": "Potts",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 240,
    "progress": 240
  },
  {
    "taskID": 241,
    "taskName": "Patrick",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 241,
    "progress": 241
  },
  {
    "taskID": 242,
    "taskName": "Brenda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 242,
    "progress": 242
  },
  {
    "taskID": 243,
    "taskName": "Lourdes",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 243,
    "progress": 243
  },
  {
    "taskID": 244,
    "taskName": "Courtney",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 244,
    "progress": 244
  },
  {
    "taskID": 245,
    "taskName": "Giles",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 245,
    "progress": 245
  },
  {
    "taskID": 246,
    "taskName": "Holloway",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 246,
    "progress": 246
  },
  {
    "taskID": 247,
    "taskName": "Christensen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 247,
    "progress": 247
  },
  {
    "taskID": 248,
    "taskName": "Hardin",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 248,
    "progress": 248
  },
  {
    "taskID": 249,
    "taskName": "Flowers",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 249,
    "progress": 249
  },
  {
    "taskID": 250,
    "taskName": "Robyn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 250,
    "progress": 250
  },
  {
    "taskID": 251,
    "taskName": "Elnora",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 251,
    "progress": 251
  },
  {
    "taskID": 252,
    "taskName": "Cortez",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 252,
    "progress": 252
  },
  {
    "taskID": 253,
    "taskName": "Lara",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 253,
    "progress": 253
  },
  {
    "taskID": 254,
    "taskName": "Kathrine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 254,
    "progress": 254
  },
  {
    "taskID": 255,
    "taskName": "Noemi",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 255,
    "progress": 255
  },
  {
    "taskID": 256,
    "taskName": "Christa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 256,
    "progress": 256
  },
  {
    "taskID": 257,
    "taskName": "Kay",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 257,
    "progress": 257
  },
  {
    "taskID": 258,
    "taskName": "Singleton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 258,
    "progress": 258
  },
  {
    "taskID": 259,
    "taskName": "Dominique",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 259,
    "progress": 259
  },
  {
    "taskID": 260,
    "taskName": "Loretta",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 260,
    "progress": 260
  },
  {
    "taskID": 261,
    "taskName": "Elizabeth",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 261,
    "progress": 261
  },
  {
    "taskID": 262,
    "taskName": "Dejesus",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 262,
    "progress": 262
  },
  {
    "taskID": 263,
    "taskName": "Willa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 263,
    "progress": 263
  },
  {
    "taskID": 264,
    "taskName": "Charlotte",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 264,
    "progress": 264
  },
  {
    "taskID": 265,
    "taskName": "Ewing",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 265,
    "progress": 265
  },
  {
    "taskID": 266,
    "taskName": "Helena",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 266,
    "progress": 266
  },
  {
    "taskID": 267,
    "taskName": "Toni",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 267,
    "progress": 267
  },
  {
    "taskID": 268,
    "taskName": "Alyce",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 268,
    "progress": 268
  },
  {
    "taskID": 269,
    "taskName": "Horton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 269,
    "progress": 269
  },
  {
    "taskID": 270,
    "taskName": "Cynthia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 270,
    "progress": 270
  },
  {
    "taskID": 271,
    "taskName": "Graham",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 271,
    "progress": 271
  },
  {
    "taskID": 272,
    "taskName": "Watts",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 272,
    "progress": 272
  },
  {
    "taskID": 273,
    "taskName": "Austin",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 273,
    "progress": 273
  },
  {
    "taskID": 274,
    "taskName": "Lester",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 274,
    "progress": 274
  },
  {
    "taskID": 275,
    "taskName": "Bentley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 275,
    "progress": 275
  },
  {
    "taskID": 276,
    "taskName": "Estes",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 276,
    "progress": 276
  },
  {
    "taskID": 277,
    "taskName": "Carrillo",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 277,
    "progress": 277
  },
  {
    "taskID": 278,
    "taskName": "Kimberley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 278,
    "progress": 278
  },
  {
    "taskID": 279,
    "taskName": "Violet",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 279,
    "progress": 279
  },
  {
    "taskID": 280,
    "taskName": "Enid",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 280,
    "progress": 280
  },
  {
    "taskID": 281,
    "taskName": "Natalie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 281,
    "progress": 281
  },
  {
    "taskID": 282,
    "taskName": "Delia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 282,
    "progress": 282
  },
  {
    "taskID": 283,
    "taskName": "Maryellen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 283,
    "progress": 283
  },
  {
    "taskID": 284,
    "taskName": "Reynolds",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 284,
    "progress": 284
  },
  {
    "taskID": 285,
    "taskName": "Mclaughlin",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 285,
    "progress": 285
  },
  {
    "taskID": 286,
    "taskName": "Zamora",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 286,
    "progress": 286
  },
  {
    "taskID": 287,
    "taskName": "Roslyn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 287,
    "progress": 287
  },
  {
    "taskID": 288,
    "taskName": "Horn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 288,
    "progress": 288
  },
  {
    "taskID": 289,
    "taskName": "Sears",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 289,
    "progress": 289
  },
  {
    "taskID": 290,
    "taskName": "Holcomb",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 290,
    "progress": 290
  },
  {
    "taskID": 291,
    "taskName": "Boone",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 291,
    "progress": 291
  },
  {
    "taskID": 292,
    "taskName": "Sandoval",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 292,
    "progress": 292
  },
  {
    "taskID": 293,
    "taskName": "Reeves",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 293,
    "progress": 293
  },
  {
    "taskID": 294,
    "taskName": "Pierce",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 294,
    "progress": 294
  },
  {
    "taskID": 295,
    "taskName": "Deana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 295,
    "progress": 295
  },
  {
    "taskID": 296,
    "taskName": "Marsha",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 296,
    "progress": 296
  },
  {
    "taskID": 297,
    "taskName": "Hallie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 297,
    "progress": 297
  },
  {
    "taskID": 298,
    "taskName": "Valarie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 298,
    "progress": 298
  },
  {
    "taskID": 299,
    "taskName": "Felicia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 299,
    "progress": 299
  },
  {
    "taskID": 300,
    "taskName": "Alvarado",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 300,
    "progress": 300
  },
  {
    "taskID": 301,
    "taskName": "Mona",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 301,
    "progress": 301
  },
  {
    "taskID": 302,
    "taskName": "Jessie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 302,
    "progress": 302
  },
  {
    "taskID": 303,
    "taskName": "Hendrix",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 303,
    "progress": 303
  },
  {
    "taskID": 304,
    "taskName": "Mavis",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 304,
    "progress": 304
  },
  {
    "taskID": 305,
    "taskName": "Ofelia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 305,
    "progress": 305
  },
  {
    "taskID": 306,
    "taskName": "Patrice",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 306,
    "progress": 306
  },
  {
    "taskID": 307,
    "taskName": "Merritt",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 307,
    "progress": 307
  },
  {
    "taskID": 308,
    "taskName": "Wilder",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 308,
    "progress": 308
  },
  {
    "taskID": 309,
    "taskName": "Tommie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 309,
    "progress": 309
  },
  {
    "taskID": 310,
    "taskName": "Saunders",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 310,
    "progress": 310
  },
  {
    "taskID": 311,
    "taskName": "Eleanor",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 311,
    "progress": 311
  },
  {
    "taskID": 312,
    "taskName": "Kinney",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 312,
    "progress": 312
  },
  {
    "taskID": 313,
    "taskName": "Tracy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 313,
    "progress": 313
  },
  {
    "taskID": 314,
    "taskName": "Wagner",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 314,
    "progress": 314
  },
  {
    "taskID": 315,
    "taskName": "Nora",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 315,
    "progress": 315
  },
  {
    "taskID": 316,
    "taskName": "Susana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 316,
    "progress": 316
  },
  {
    "taskID": 317,
    "taskName": "Branch",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 317,
    "progress": 317
  },
  {
    "taskID": 318,
    "taskName": "Leon",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 318,
    "progress": 318
  },
  {
    "taskID": 319,
    "taskName": "Merrill",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 319,
    "progress": 319
  },
  {
    "taskID": 320,
    "taskName": "Nona",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 320,
    "progress": 320
  },
  {
    "taskID": 321,
    "taskName": "Woods",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 321,
    "progress": 321
  },
  {
    "taskID": 322,
    "taskName": "Blanchard",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 322,
    "progress": 322
  },
  {
    "taskID": 323,
    "taskName": "Mable",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 323,
    "progress": 323
  },
  {
    "taskID": 324,
    "taskName": "Susanna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 324,
    "progress": 324
  },
  {
    "taskID": 325,
    "taskName": "Michelle",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 325,
    "progress": 325
  },
  {
    "taskID": 326,
    "taskName": "Gay",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 326,
    "progress": 326
  },
  {
    "taskID": 327,
    "taskName": "Fran",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 327,
    "progress": 327
  },
  {
    "taskID": 328,
    "taskName": "Jayne",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 328,
    "progress": 328
  },
  {
    "taskID": 329,
    "taskName": "Lenora",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 329,
    "progress": 329
  },
  {
    "taskID": 330,
    "taskName": "Rosanne",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 330,
    "progress": 330
  },
  {
    "taskID": 331,
    "taskName": "Sybil",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 331,
    "progress": 331
  },
  {
    "taskID": 332,
    "taskName": "Iris",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 332,
    "progress": 332
  },
  {
    "taskID": 333,
    "taskName": "Church",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 333,
    "progress": 333
  },
  {
    "taskID": 334,
    "taskName": "Jodi",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 334,
    "progress": 334
  },
  {
    "taskID": 335,
    "taskName": "Corrine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 335,
    "progress": 335
  },
  {
    "taskID": 336,
    "taskName": "Sheryl",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 336,
    "progress": 336
  },
  {
    "taskID": 337,
    "taskName": "Rosario",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 337,
    "progress": 337
  },
  {
    "taskID": 338,
    "taskName": "Bradford",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 338,
    "progress": 338
  },
  {
    "taskID": 339,
    "taskName": "Stevens",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 339,
    "progress": 339
  },
  {
    "taskID": 340,
    "taskName": "Kim",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 340,
    "progress": 340
  },
  {
    "taskID": 341,
    "taskName": "Lorraine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 341,
    "progress": 341
  },
  {
    "taskID": 342,
    "taskName": "Augusta",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 342,
    "progress": 342
  },
  {
    "taskID": 343,
    "taskName": "Cleo",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 343,
    "progress": 343
  },
  {
    "taskID": 344,
    "taskName": "Mccray",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 344,
    "progress": 344
  },
  {
    "taskID": 345,
    "taskName": "Harriet",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 345,
    "progress": 345
  },
  {
    "taskID": 346,
    "taskName": "Caroline",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 346,
    "progress": 346
  },
  {
    "taskID": 347,
    "taskName": "Geraldine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 347,
    "progress": 347
  },
  {
    "taskID": 348,
    "taskName": "Brock",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 348,
    "progress": 348
  },
  {
    "taskID": 349,
    "taskName": "Ella",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 349,
    "progress": 349
  },
  {
    "taskID": 350,
    "taskName": "Hughes",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 350,
    "progress": 350
  },
  {
    "taskID": 351,
    "taskName": "Ethel",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 351,
    "progress": 351
  },
  {
    "taskID": 352,
    "taskName": "Chaney",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 352,
    "progress": 352
  },
  {
    "taskID": 353,
    "taskName": "Goldie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 353,
    "progress": 353
  },
  {
    "taskID": 354,
    "taskName": "Montgomery",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 354,
    "progress": 354
  },
  {
    "taskID": 355,
    "taskName": "Terrell",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 355,
    "progress": 355
  },
  {
    "taskID": 356,
    "taskName": "Barlow",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 356,
    "progress": 356
  },
  {
    "taskID": 357,
    "taskName": "Marguerite",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 357,
    "progress": 357
  },
  {
    "taskID": 358,
    "taskName": "Nettie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 358,
    "progress": 358
  },
  {
    "taskID": 359,
    "taskName": "Lindsey",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 359,
    "progress": 359
  },
  {
    "taskID": 360,
    "taskName": "Powers",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 360,
    "progress": 360
  },
  {
    "taskID": 361,
    "taskName": "Hampton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 361,
    "progress": 361
  },
  {
    "taskID": 362,
    "taskName": "Arline",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 362,
    "progress": 362
  },
  {
    "taskID": 363,
    "taskName": "Alissa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 363,
    "progress": 363
  },
  {
    "taskID": 364,
    "taskName": "Jerry",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 364,
    "progress": 364
  },
  {
    "taskID": 365,
    "taskName": "Middleton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 365,
    "progress": 365
  },
  {
    "taskID": 366,
    "taskName": "Whitfield",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 366,
    "progress": 366
  },
  {
    "taskID": 367,
    "taskName": "Adkins",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 367,
    "progress": 367
  },
  {
    "taskID": 368,
    "taskName": "Rocha",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 368,
    "progress": 368
  },
  {
    "taskID": 369,
    "taskName": "Meredith",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 369,
    "progress": 369
  },
  {
    "taskID": 370,
    "taskName": "Gates",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 370,
    "progress": 370
  },
  {
    "taskID": 371,
    "taskName": "Leila",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 371,
    "progress": 371
  },
  {
    "taskID": 372,
    "taskName": "Richard",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 372,
    "progress": 372
  },
  {
    "taskID": 373,
    "taskName": "Harper",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 373,
    "progress": 373
  },
  {
    "taskID": 374,
    "taskName": "Baird",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 374,
    "progress": 374
  },
  {
    "taskID": 375,
    "taskName": "Queen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 375,
    "progress": 375
  },
  {
    "taskID": 376,
    "taskName": "Mercado",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 376,
    "progress": 376
  },
  {
    "taskID": 377,
    "taskName": "Amber",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 377,
    "progress": 377
  },
  {
    "taskID": 378,
    "taskName": "Marla",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 378,
    "progress": 378
  },
  {
    "taskID": 379,
    "taskName": "Hickman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 379,
    "progress": 379
  },
  {
    "taskID": 380,
    "taskName": "Becky",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 380,
    "progress": 380
  },
  {
    "taskID": 381,
    "taskName": "Weaver",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 381,
    "progress": 381
  },
  {
    "taskID": 382,
    "taskName": "Pearl",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 382,
    "progress": 382
  },
  {
    "taskID": 383,
    "taskName": "Christi",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 383,
    "progress": 383
  },
  {
    "taskID": 384,
    "taskName": "Mcdowell",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 384,
    "progress": 384
  },
  {
    "taskID": 385,
    "taskName": "Michael",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 385,
    "progress": 385
  },
  {
    "taskID": 386,
    "taskName": "Sawyer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 386,
    "progress": 386
  },
  {
    "taskID": 387,
    "taskName": "Hillary",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 387,
    "progress": 387
  },
  {
    "taskID": 388,
    "taskName": "Christian",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 388,
    "progress": 388
  },
  {
    "taskID": 389,
    "taskName": "Florine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 389,
    "progress": 389
  },
  {
    "taskID": 390,
    "taskName": "Spencer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 390,
    "progress": 390
  },
  {
    "taskID": 391,
    "taskName": "Hernandez",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 391,
    "progress": 391
  },
  {
    "taskID": 392,
    "taskName": "Mckenzie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 392,
    "progress": 392
  },
  {
    "taskID": 393,
    "taskName": "Luella",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 393,
    "progress": 393
  },
  {
    "taskID": 394,
    "taskName": "Marcie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 394,
    "progress": 394
  },
  {
    "taskID": 395,
    "taskName": "Collier",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 395,
    "progress": 395
  },
  {
    "taskID": 396,
    "taskName": "Irma",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 396,
    "progress": 396
  },
  {
    "taskID": 397,
    "taskName": "Lesley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 397,
    "progress": 397
  },
  {
    "taskID": 398,
    "taskName": "Shawn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 398,
    "progress": 398
  },
  {
    "taskID": 399,
    "taskName": "Justine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 399,
    "progress": 399
  },
  {
    "taskID": 400,
    "taskName": "Myrtle",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 400,
    "progress": 400
  },
  {
    "taskID": 401,
    "taskName": "Mullins",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 401,
    "progress": 401
  },
  {
    "taskID": 402,
    "taskName": "Katie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 402,
    "progress": 402
  },
  {
    "taskID": 403,
    "taskName": "Bradley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 403,
    "progress": 403
  },
  {
    "taskID": 404,
    "taskName": "Tamra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 404,
    "progress": 404
  },
  {
    "taskID": 405,
    "taskName": "Baxter",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 405,
    "progress": 405
  },
  {
    "taskID": 406,
    "taskName": "Lindsey",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 406,
    "progress": 406
  },
  {
    "taskID": 407,
    "taskName": "Rowland",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 407,
    "progress": 407
  },
  {
    "taskID": 408,
    "taskName": "Delgado",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 408,
    "progress": 408
  },
  {
    "taskID": 409,
    "taskName": "Cherie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 409,
    "progress": 409
  },
  {
    "taskID": 410,
    "taskName": "Davidson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 410,
    "progress": 410
  },
  {
    "taskID": 411,
    "taskName": "Katina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 411,
    "progress": 411
  },
  {
    "taskID": 412,
    "taskName": "Alta",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 412,
    "progress": 412
  },
  {
    "taskID": 413,
    "taskName": "Darlene",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 413,
    "progress": 413
  },
  {
    "taskID": 414,
    "taskName": "Byers",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 414,
    "progress": 414
  },
  {
    "taskID": 415,
    "taskName": "Mccarty",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 415,
    "progress": 415
  },
  {
    "taskID": 416,
    "taskName": "Nielsen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 416,
    "progress": 416
  },
  {
    "taskID": 417,
    "taskName": "Gould",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 417,
    "progress": 417
  },
  {
    "taskID": 418,
    "taskName": "Milagros",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 418,
    "progress": 418
  },
  {
    "taskID": 419,
    "taskName": "Leanne",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 419,
    "progress": 419
  },
  {
    "taskID": 420,
    "taskName": "Jimenez",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 420,
    "progress": 420
  },
  {
    "taskID": 421,
    "taskName": "Martha",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 421,
    "progress": 421
  },
  {
    "taskID": 422,
    "taskName": "Diaz",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 422,
    "progress": 422
  },
  {
    "taskID": 423,
    "taskName": "Crystal",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 423,
    "progress": 423
  },
  {
    "taskID": 424,
    "taskName": "Josie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 424,
    "progress": 424
  },
  {
    "taskID": 425,
    "taskName": "Chapman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 425,
    "progress": 425
  },
  {
    "taskID": 426,
    "taskName": "Ines",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 426,
    "progress": 426
  },
  {
    "taskID": 427,
    "taskName": "Angelica",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 427,
    "progress": 427
  },
  {
    "taskID": 428,
    "taskName": "Cecilia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 428,
    "progress": 428
  },
  {
    "taskID": 429,
    "taskName": "Clara",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 429,
    "progress": 429
  },
  {
    "taskID": 430,
    "taskName": "Wallace",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 430,
    "progress": 430
  },
  {
    "taskID": 431,
    "taskName": "Sophie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 431,
    "progress": 431
  },
  {
    "taskID": 432,
    "taskName": "Shaffer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 432,
    "progress": 432
  },
  {
    "taskID": 433,
    "taskName": "Mitzi",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 433,
    "progress": 433
  },
  {
    "taskID": 434,
    "taskName": "George",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 434,
    "progress": 434
  },
  {
    "taskID": 435,
    "taskName": "Georgia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 435,
    "progress": 435
  },
  {
    "taskID": 436,
    "taskName": "Nina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 436,
    "progress": 436
  },
  {
    "taskID": 437,
    "taskName": "Avila",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 437,
    "progress": 437
  },
  {
    "taskID": 438,
    "taskName": "Bailey",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 438,
    "progress": 438
  },
  {
    "taskID": 439,
    "taskName": "Ward",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 439,
    "progress": 439
  },
  {
    "taskID": 440,
    "taskName": "Fuentes",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 440,
    "progress": 440
  },
  {
    "taskID": 441,
    "taskName": "Simmons",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 441,
    "progress": 441
  },
  {
    "taskID": 442,
    "taskName": "Roy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 442,
    "progress": 442
  },
  {
    "taskID": 443,
    "taskName": "Evangelina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 443,
    "progress": 443
  },
  {
    "taskID": 444,
    "taskName": "Nanette",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 444,
    "progress": 444
  },
  {
    "taskID": 445,
    "taskName": "Crane",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 445,
    "progress": 445
  },
  {
    "taskID": 446,
    "taskName": "Kara",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 446,
    "progress": 446
  },
  {
    "taskID": 447,
    "taskName": "Hattie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 447,
    "progress": 447
  },
  {
    "taskID": 448,
    "taskName": "Charity",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 448,
    "progress": 448
  },
  {
    "taskID": 449,
    "taskName": "Rachael",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 449,
    "progress": 449
  },
  {
    "taskID": 450,
    "taskName": "Sally",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 450,
    "progress": 450
  },
  {
    "taskID": 451,
    "taskName": "Earnestine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 451,
    "progress": 451
  },
  {
    "taskID": 452,
    "taskName": "Porter",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 452,
    "progress": 452
  },
  {
    "taskID": 453,
    "taskName": "Lora",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 453,
    "progress": 453
  },
  {
    "taskID": 454,
    "taskName": "Reilly",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 454,
    "progress": 454
  },
  {
    "taskID": 455,
    "taskName": "Abbott",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 455,
    "progress": 455
  },
  {
    "taskID": 456,
    "taskName": "Aguilar",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 456,
    "progress": 456
  },
  {
    "taskID": 457,
    "taskName": "Noelle",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 457,
    "progress": 457
  },
  {
    "taskID": 458,
    "taskName": "Williams",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 458,
    "progress": 458
  },
  {
    "taskID": 459,
    "taskName": "Desiree",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 459,
    "progress": 459
  },
  {
    "taskID": 460,
    "taskName": "Eddie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 460,
    "progress": 460
  },
  {
    "taskID": 461,
    "taskName": "Elisa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 461,
    "progress": 461
  },
  {
    "taskID": 462,
    "taskName": "Matthews",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 462,
    "progress": 462
  },
  {
    "taskID": 463,
    "taskName": "Shannon",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 463,
    "progress": 463
  },
  {
    "taskID": 464,
    "taskName": "Torres",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 464,
    "progress": 464
  },
  {
    "taskID": 465,
    "taskName": "Debbie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 465,
    "progress": 465
  },
  {
    "taskID": 466,
    "taskName": "Marcella",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 466,
    "progress": 466
  },
  {
    "taskID": 467,
    "taskName": "Elaine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 467,
    "progress": 467
  },
  {
    "taskID": 468,
    "taskName": "Vaughan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 468,
    "progress": 468
  },
  {
    "taskID": 469,
    "taskName": "Dalton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 469,
    "progress": 469
  },
  {
    "taskID": 470,
    "taskName": "Moss",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 470,
    "progress": 470
  },
  {
    "taskID": 471,
    "taskName": "Hester",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 471,
    "progress": 471
  },
  {
    "taskID": 472,
    "taskName": "Dixie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 472,
    "progress": 472
  },
  {
    "taskID": 473,
    "taskName": "Sherrie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 473,
    "progress": 473
  },
  {
    "taskID": 474,
    "taskName": "Kelsey",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 474,
    "progress": 474
  },
  {
    "taskID": 475,
    "taskName": "Petersen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 475,
    "progress": 475
  },
  {
    "taskID": 476,
    "taskName": "Ophelia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 476,
    "progress": 476
  },
  {
    "taskID": 477,
    "taskName": "Bright",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 477,
    "progress": 477
  },
  {
    "taskID": 478,
    "taskName": "Blanche",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 478,
    "progress": 478
  },
  {
    "taskID": 479,
    "taskName": "Larsen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 479,
    "progress": 479
  },
  {
    "taskID": 480,
    "taskName": "Marisa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 480,
    "progress": 480
  },
  {
    "taskID": 481,
    "taskName": "Dianna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 481,
    "progress": 481
  },
  {
    "taskID": 482,
    "taskName": "Hubbard",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 482,
    "progress": 482
  },
  {
    "taskID": 483,
    "taskName": "Delores",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 483,
    "progress": 483
  },
  {
    "taskID": 484,
    "taskName": "Mcguire",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 484,
    "progress": 484
  },
  {
    "taskID": 485,
    "taskName": "Josefa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 485,
    "progress": 485
  },
  {
    "taskID": 486,
    "taskName": "Bernadine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 486,
    "progress": 486
  },
  {
    "taskID": 487,
    "taskName": "Darcy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 487,
    "progress": 487
  },
  {
    "taskID": 488,
    "taskName": "April",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 488,
    "progress": 488
  },
  {
    "taskID": 489,
    "taskName": "Marci",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 489,
    "progress": 489
  },
  {
    "taskID": 490,
    "taskName": "Ilene",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 490,
    "progress": 490
  },
  {
    "taskID": 491,
    "taskName": "Franklin",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 491,
    "progress": 491
  },
  {
    "taskID": 492,
    "taskName": "Hoffman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 492,
    "progress": 492
  },
  {
    "taskID": 493,
    "taskName": "Strickland",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 493,
    "progress": 493
  },
  {
    "taskID": 494,
    "taskName": "Wiley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 494,
    "progress": 494
  },
  {
    "taskID": 495,
    "taskName": "Atkinson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 495,
    "progress": 495
  },
  {
    "taskID": 496,
    "taskName": "Lacy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 496,
    "progress": 496
  },
  {
    "taskID": 497,
    "taskName": "Freda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 497,
    "progress": 497
  },
  {
    "taskID": 498,
    "taskName": "Erika",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 498,
    "progress": 498
  },
  {
    "taskID": 499,
    "taskName": "Delaney",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 499,
    "progress": 499
  },
  {
    "taskID": 500,
    "taskName": "Velma",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 500,
    "progress": 500
  },
  {
    "taskID": 501,
    "taskName": "Jocelyn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 501,
    "progress": 501
  },
  {
    "taskID": 502,
    "taskName": "Cheri",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 502,
    "progress": 502
  },
  {
    "taskID": 503,
    "taskName": "Miriam",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 503,
    "progress": 503
  },
  {
    "taskID": 504,
    "taskName": "Minerva",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 504,
    "progress": 504
  },
  {
    "taskID": 505,
    "taskName": "Owen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 505,
    "progress": 505
  },
  {
    "taskID": 506,
    "taskName": "Alison",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 506,
    "progress": 506
  },
  {
    "taskID": 507,
    "taskName": "Esther",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 507,
    "progress": 507
  },
  {
    "taskID": 508,
    "taskName": "Silvia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 508,
    "progress": 508
  },
  {
    "taskID": 509,
    "taskName": "Kline",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 509,
    "progress": 509
  },
  {
    "taskID": 510,
    "taskName": "Frieda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 510,
    "progress": 510
  },
  {
    "taskID": 511,
    "taskName": "Adams",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 511,
    "progress": 511
  },
  {
    "taskID": 512,
    "taskName": "Lakisha",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 512,
    "progress": 512
  },
  {
    "taskID": 513,
    "taskName": "Rosales",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 513,
    "progress": 513
  },
  {
    "taskID": 514,
    "taskName": "Cotton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 514,
    "progress": 514
  },
  {
    "taskID": 515,
    "taskName": "Mcmillan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 515,
    "progress": 515
  },
  {
    "taskID": 516,
    "taskName": "Bishop",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 516,
    "progress": 516
  },
  {
    "taskID": 517,
    "taskName": "Beverley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 517,
    "progress": 517
  },
  {
    "taskID": 518,
    "taskName": "Gomez",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 518,
    "progress": 518
  },
  {
    "taskID": 519,
    "taskName": "Sylvia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 519,
    "progress": 519
  },
  {
    "taskID": 520,
    "taskName": "Gaines",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 520,
    "progress": 520
  },
  {
    "taskID": 521,
    "taskName": "Taylor",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 521,
    "progress": 521
  },
  {
    "taskID": 522,
    "taskName": "Myrna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 522,
    "progress": 522
  },
  {
    "taskID": 523,
    "taskName": "Rita",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 523,
    "progress": 523
  },
  {
    "taskID": 524,
    "taskName": "Newman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 524,
    "progress": 524
  },
  {
    "taskID": 525,
    "taskName": "Sherry",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 525,
    "progress": 525
  },
  {
    "taskID": 526,
    "taskName": "Clarice",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 526,
    "progress": 526
  },
  {
    "taskID": 527,
    "taskName": "Emerson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 527,
    "progress": 527
  },
  {
    "taskID": 528,
    "taskName": "Valencia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 528,
    "progress": 528
  },
  {
    "taskID": 529,
    "taskName": "Alyson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 529,
    "progress": 529
  },
  {
    "taskID": 530,
    "taskName": "Duran",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 530,
    "progress": 530
  },
  {
    "taskID": 531,
    "taskName": "Blackburn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 531,
    "progress": 531
  },
  {
    "taskID": 532,
    "taskName": "Shepherd",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 532,
    "progress": 532
  },
  {
    "taskID": 533,
    "taskName": "Mccall",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 533,
    "progress": 533
  },
  {
    "taskID": 534,
    "taskName": "Alejandra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 534,
    "progress": 534
  },
  {
    "taskID": 535,
    "taskName": "Hart",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 535,
    "progress": 535
  },
  {
    "taskID": 536,
    "taskName": "Rosalyn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 536,
    "progress": 536
  },
  {
    "taskID": 537,
    "taskName": "Lila",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 537,
    "progress": 537
  },
  {
    "taskID": 538,
    "taskName": "Rosemary",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 538,
    "progress": 538
  },
  {
    "taskID": 539,
    "taskName": "Glenda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 539,
    "progress": 539
  },
  {
    "taskID": 540,
    "taskName": "Langley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 540,
    "progress": 540
  },
  {
    "taskID": 541,
    "taskName": "Tami",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 541,
    "progress": 541
  },
  {
    "taskID": 542,
    "taskName": "Haley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 542,
    "progress": 542
  },
  {
    "taskID": 543,
    "taskName": "Tyson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 543,
    "progress": 543
  },
  {
    "taskID": 544,
    "taskName": "Chris",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 544,
    "progress": 544
  },
  {
    "taskID": 545,
    "taskName": "Miles",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 545,
    "progress": 545
  },
  {
    "taskID": 546,
    "taskName": "Freida",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 546,
    "progress": 546
  },
  {
    "taskID": 547,
    "taskName": "Padilla",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 547,
    "progress": 547
  },
  {
    "taskID": 548,
    "taskName": "Thornton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 548,
    "progress": 548
  },
  {
    "taskID": 549,
    "taskName": "Rose",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 549,
    "progress": 549
  },
  {
    "taskID": 550,
    "taskName": "Barnes",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 550,
    "progress": 550
  },
  {
    "taskID": 551,
    "taskName": "Pace",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 551,
    "progress": 551
  },
  {
    "taskID": 552,
    "taskName": "Cummings",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 552,
    "progress": 552
  },
  {
    "taskID": 553,
    "taskName": "Chase",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 553,
    "progress": 553
  },
  {
    "taskID": 554,
    "taskName": "Manning",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 554,
    "progress": 554
  },
  {
    "taskID": 555,
    "taskName": "Estelle",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 555,
    "progress": 555
  },
  {
    "taskID": 556,
    "taskName": "Hurley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 556,
    "progress": 556
  },
  {
    "taskID": 557,
    "taskName": "Rosella",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 557,
    "progress": 557
  },
  {
    "taskID": 558,
    "taskName": "Riddle",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 558,
    "progress": 558
  },
  {
    "taskID": 559,
    "taskName": "Crosby",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 559,
    "progress": 559
  },
  {
    "taskID": 560,
    "taskName": "Gladys",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 560,
    "progress": 560
  },
  {
    "taskID": 561,
    "taskName": "May",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 561,
    "progress": 561
  },
  {
    "taskID": 562,
    "taskName": "Hogan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 562,
    "progress": 562
  },
  {
    "taskID": 563,
    "taskName": "Gilmore",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 563,
    "progress": 563
  },
  {
    "taskID": 564,
    "taskName": "Nguyen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 564,
    "progress": 564
  },
  {
    "taskID": 565,
    "taskName": "Leticia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 565,
    "progress": 565
  },
  {
    "taskID": 566,
    "taskName": "Lina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 566,
    "progress": 566
  },
  {
    "taskID": 567,
    "taskName": "Booth",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 567,
    "progress": 567
  },
  {
    "taskID": 568,
    "taskName": "Nichole",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 568,
    "progress": 568
  },
  {
    "taskID": 569,
    "taskName": "Rochelle",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 569,
    "progress": 569
  },
  {
    "taskID": 570,
    "taskName": "Jessica",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 570,
    "progress": 570
  },
  {
    "taskID": 571,
    "taskName": "Odessa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 571,
    "progress": 571
  },
  {
    "taskID": 572,
    "taskName": "Donna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 572,
    "progress": 572
  },
  {
    "taskID": 573,
    "taskName": "Angeline",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 573,
    "progress": 573
  },
  {
    "taskID": 574,
    "taskName": "Kaye",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 574,
    "progress": 574
  },
  {
    "taskID": 575,
    "taskName": "Hazel",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 575,
    "progress": 575
  },
  {
    "taskID": 576,
    "taskName": "Barron",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 576,
    "progress": 576
  },
  {
    "taskID": 577,
    "taskName": "Sallie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 577,
    "progress": 577
  },
  {
    "taskID": 578,
    "taskName": "Reyna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 578,
    "progress": 578
  },
  {
    "taskID": 579,
    "taskName": "Rhea",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 579,
    "progress": 579
  },
  {
    "taskID": 580,
    "taskName": "Lynn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 580,
    "progress": 580
  },
  {
    "taskID": 581,
    "taskName": "Raymond",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 581,
    "progress": 581
  },
  {
    "taskID": 582,
    "taskName": "Charmaine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 582,
    "progress": 582
  },
  {
    "taskID": 583,
    "taskName": "Caitlin",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 583,
    "progress": 583
  },
  {
    "taskID": 584,
    "taskName": "Wendy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 584,
    "progress": 584
  },
  {
    "taskID": 585,
    "taskName": "Gross",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 585,
    "progress": 585
  },
  {
    "taskID": 586,
    "taskName": "Kennedy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 586,
    "progress": 586
  },
  {
    "taskID": 587,
    "taskName": "Mcknight",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 587,
    "progress": 587
  },
  {
    "taskID": 588,
    "taskName": "Cristina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 588,
    "progress": 588
  },
  {
    "taskID": 589,
    "taskName": "Mullen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 589,
    "progress": 589
  },
  {
    "taskID": 590,
    "taskName": "Colette",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 590,
    "progress": 590
  },
  {
    "taskID": 591,
    "taskName": "Burns",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 591,
    "progress": 591
  },
  {
    "taskID": 592,
    "taskName": "Marissa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 592,
    "progress": 592
  },
  {
    "taskID": 593,
    "taskName": "Tanya",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 593,
    "progress": 593
  },
  {
    "taskID": 594,
    "taskName": "Fischer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 594,
    "progress": 594
  },
  {
    "taskID": 595,
    "taskName": "Barrett",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 595,
    "progress": 595
  },
  {
    "taskID": 596,
    "taskName": "Janie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 596,
    "progress": 596
  },
  {
    "taskID": 597,
    "taskName": "Veronica",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 597,
    "progress": 597
  },
  {
    "taskID": 598,
    "taskName": "Ryan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 598,
    "progress": 598
  },
  {
    "taskID": 599,
    "taskName": "Lara",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 599,
    "progress": 599
  },
  {
    "taskID": 600,
    "taskName": "Turner",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 600,
    "progress": 600
  },
  {
    "taskID": 601,
    "taskName": "Adrian",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 601,
    "progress": 601
  },
  {
    "taskID": 602,
    "taskName": "Erna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 602,
    "progress": 602
  },
  {
    "taskID": 603,
    "taskName": "Stuart",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 603,
    "progress": 603
  },
  {
    "taskID": 604,
    "taskName": "Angelita",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 604,
    "progress": 604
  },
  {
    "taskID": 605,
    "taskName": "Mccullough",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 605,
    "progress": 605
  },
  {
    "taskID": 606,
    "taskName": "Glover",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 606,
    "progress": 606
  },
  {
    "taskID": 607,
    "taskName": "Gibbs",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 607,
    "progress": 607
  },
  {
    "taskID": 608,
    "taskName": "Maryann",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 608,
    "progress": 608
  },
  {
    "taskID": 609,
    "taskName": "Parrish",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 609,
    "progress": 609
  },
  {
    "taskID": 610,
    "taskName": "Mercedes",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 610,
    "progress": 610
  },
  {
    "taskID": 611,
    "taskName": "Nieves",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 611,
    "progress": 611
  },
  {
    "taskID": 612,
    "taskName": "Melva",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 612,
    "progress": 612
  },
  {
    "taskID": 613,
    "taskName": "Kayla",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 613,
    "progress": 613
  },
  {
    "taskID": 614,
    "taskName": "Stacy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 614,
    "progress": 614
  },
  {
    "taskID": 615,
    "taskName": "Vance",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 615,
    "progress": 615
  },
  {
    "taskID": 616,
    "taskName": "Victoria",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 616,
    "progress": 616
  },
  {
    "taskID": 617,
    "taskName": "Allyson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 617,
    "progress": 617
  },
  {
    "taskID": 618,
    "taskName": "Mara",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 618,
    "progress": 618
  },
  {
    "taskID": 619,
    "taskName": "Daisy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 619,
    "progress": 619
  },
  {
    "taskID": 620,
    "taskName": "Merle",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 620,
    "progress": 620
  },
  {
    "taskID": 621,
    "taskName": "Shari",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 621,
    "progress": 621
  },
  {
    "taskID": 622,
    "taskName": "Herman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 622,
    "progress": 622
  },
  {
    "taskID": 623,
    "taskName": "Hardy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 623,
    "progress": 623
  },
  {
    "taskID": 624,
    "taskName": "Weiss",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 624,
    "progress": 624
  },
  {
    "taskID": 625,
    "taskName": "Boyer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 625,
    "progress": 625
  },
  {
    "taskID": 626,
    "taskName": "Tia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 626,
    "progress": 626
  },
  {
    "taskID": 627,
    "taskName": "Neal",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 627,
    "progress": 627
  },
  {
    "taskID": 628,
    "taskName": "Rosa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 628,
    "progress": 628
  },
  {
    "taskID": 629,
    "taskName": "Griffin",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 629,
    "progress": 629
  },
  {
    "taskID": 630,
    "taskName": "Jarvis",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 630,
    "progress": 630
  },
  {
    "taskID": 631,
    "taskName": "Loraine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 631,
    "progress": 631
  },
  {
    "taskID": 632,
    "taskName": "Lucas",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 632,
    "progress": 632
  },
  {
    "taskID": 633,
    "taskName": "Elva",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 633,
    "progress": 633
  },
  {
    "taskID": 634,
    "taskName": "Marylou",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 634,
    "progress": 634
  },
  {
    "taskID": 635,
    "taskName": "Sanford",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 635,
    "progress": 635
  },
  {
    "taskID": 636,
    "taskName": "Vasquez",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 636,
    "progress": 636
  },
  {
    "taskID": 637,
    "taskName": "Casey",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 637,
    "progress": 637
  },
  {
    "taskID": 638,
    "taskName": "Ila",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 638,
    "progress": 638
  },
  {
    "taskID": 639,
    "taskName": "Tamika",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 639,
    "progress": 639
  },
  {
    "taskID": 640,
    "taskName": "Gay",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 640,
    "progress": 640
  },
  {
    "taskID": 641,
    "taskName": "Malinda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 641,
    "progress": 641
  },
  {
    "taskID": 642,
    "taskName": "Mathis",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 642,
    "progress": 642
  },
  {
    "taskID": 643,
    "taskName": "Leonor",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 643,
    "progress": 643
  },
  {
    "taskID": 644,
    "taskName": "Misty",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 644,
    "progress": 644
  },
  {
    "taskID": 645,
    "taskName": "Benton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 645,
    "progress": 645
  },
  {
    "taskID": 646,
    "taskName": "Elliott",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 646,
    "progress": 646
  },
  {
    "taskID": 647,
    "taskName": "Banks",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 647,
    "progress": 647
  },
  {
    "taskID": 648,
    "taskName": "Frank",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 648,
    "progress": 648
  },
  {
    "taskID": 649,
    "taskName": "Sellers",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 649,
    "progress": 649
  },
  {
    "taskID": 650,
    "taskName": "Bauer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 650,
    "progress": 650
  },
  {
    "taskID": 651,
    "taskName": "Vega",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 651,
    "progress": 651
  },
  {
    "taskID": 652,
    "taskName": "Burch",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 652,
    "progress": 652
  },
  {
    "taskID": 653,
    "taskName": "Leann",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 653,
    "progress": 653
  },
  {
    "taskID": 654,
    "taskName": "Salazar",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 654,
    "progress": 654
  },
  {
    "taskID": 655,
    "taskName": "Booker",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 655,
    "progress": 655
  },
  {
    "taskID": 656,
    "taskName": "Hayes",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 656,
    "progress": 656
  },
  {
    "taskID": 657,
    "taskName": "Mayra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 657,
    "progress": 657
  },
  {
    "taskID": 658,
    "taskName": "Hall",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 658,
    "progress": 658
  },
  {
    "taskID": 659,
    "taskName": "Pickett",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 659,
    "progress": 659
  },
  {
    "taskID": 660,
    "taskName": "Lang",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 660,
    "progress": 660
  },
  {
    "taskID": 661,
    "taskName": "Huffman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 661,
    "progress": 661
  },
  {
    "taskID": 662,
    "taskName": "Juliet",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 662,
    "progress": 662
  },
  {
    "taskID": 663,
    "taskName": "Alexander",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 663,
    "progress": 663
  },
  {
    "taskID": 664,
    "taskName": "Townsend",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 664,
    "progress": 664
  },
  {
    "taskID": 665,
    "taskName": "Heath",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 665,
    "progress": 665
  },
  {
    "taskID": 666,
    "taskName": "Monroe",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 666,
    "progress": 666
  },
  {
    "taskID": 667,
    "taskName": "Hanson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 667,
    "progress": 667
  },
  {
    "taskID": 668,
    "taskName": "Abigail",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 668,
    "progress": 668
  },
  {
    "taskID": 669,
    "taskName": "Selma",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 669,
    "progress": 669
  },
  {
    "taskID": 670,
    "taskName": "Kari",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 670,
    "progress": 670
  },
  {
    "taskID": 671,
    "taskName": "Sweet",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 671,
    "progress": 671
  },
  {
    "taskID": 672,
    "taskName": "Rosalinda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 672,
    "progress": 672
  },
  {
    "taskID": 673,
    "taskName": "Aimee",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 673,
    "progress": 673
  },
  {
    "taskID": 674,
    "taskName": "Marlene",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 674,
    "progress": 674
  },
  {
    "taskID": 675,
    "taskName": "Mariana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 675,
    "progress": 675
  },
  {
    "taskID": 676,
    "taskName": "Snyder",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 676,
    "progress": 676
  },
  {
    "taskID": 677,
    "taskName": "Witt",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 677,
    "progress": 677
  },
  {
    "taskID": 678,
    "taskName": "Alba",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 678,
    "progress": 678
  },
  {
    "taskID": 679,
    "taskName": "Clark",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 679,
    "progress": 679
  },
  {
    "taskID": 680,
    "taskName": "Dollie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 680,
    "progress": 680
  },
  {
    "taskID": 681,
    "taskName": "Karina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 681,
    "progress": 681
  },
  {
    "taskID": 682,
    "taskName": "Wells",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 682,
    "progress": 682
  },
  {
    "taskID": 683,
    "taskName": "Avis",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 683,
    "progress": 683
  },
  {
    "taskID": 684,
    "taskName": "Pittman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 684,
    "progress": 684
  },
  {
    "taskID": 685,
    "taskName": "Hewitt",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 685,
    "progress": 685
  },
  {
    "taskID": 686,
    "taskName": "Rowe",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 686,
    "progress": 686
  },
  {
    "taskID": 687,
    "taskName": "Terrie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 687,
    "progress": 687
  },
  {
    "taskID": 688,
    "taskName": "Kristina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 688,
    "progress": 688
  },
  {
    "taskID": 689,
    "taskName": "Dillon",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 689,
    "progress": 689
  },
  {
    "taskID": 690,
    "taskName": "Blankenship",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 690,
    "progress": 690
  },
  {
    "taskID": 691,
    "taskName": "Therese",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 691,
    "progress": 691
  },
  {
    "taskID": 692,
    "taskName": "Autumn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 692,
    "progress": 692
  },
  {
    "taskID": 693,
    "taskName": "Marcia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 693,
    "progress": 693
  },
  {
    "taskID": 694,
    "taskName": "Floyd",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 694,
    "progress": 694
  },
  {
    "taskID": 695,
    "taskName": "Nolan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 695,
    "progress": 695
  },
  {
    "taskID": 696,
    "taskName": "Malone",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 696,
    "progress": 696
  },
  {
    "taskID": 697,
    "taskName": "Whitehead",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 697,
    "progress": 697
  },
  {
    "taskID": 698,
    "taskName": "Coffey",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 698,
    "progress": 698
  },
  {
    "taskID": 699,
    "taskName": "Karyn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 699,
    "progress": 699
  },
  {
    "taskID": 700,
    "taskName": "Carpenter",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 700,
    "progress": 700
  },
  {
    "taskID": 701,
    "taskName": "Janis",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 701,
    "progress": 701
  },
  {
    "taskID": 702,
    "taskName": "Love",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 702,
    "progress": 702
  },
  {
    "taskID": 703,
    "taskName": "Rene",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 703,
    "progress": 703
  },
  {
    "taskID": 704,
    "taskName": "Eileen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 704,
    "progress": 704
  },
  {
    "taskID": 705,
    "taskName": "Anna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 705,
    "progress": 705
  },
  {
    "taskID": 706,
    "taskName": "Sheena",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 706,
    "progress": 706
  },
  {
    "taskID": 707,
    "taskName": "Bernice",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 707,
    "progress": 707
  },
  {
    "taskID": 708,
    "taskName": "Eva",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 708,
    "progress": 708
  },
  {
    "taskID": 709,
    "taskName": "Imelda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 709,
    "progress": 709
  },
  {
    "taskID": 710,
    "taskName": "Bowers",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 710,
    "progress": 710
  },
  {
    "taskID": 711,
    "taskName": "Oconnor",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 711,
    "progress": 711
  },
  {
    "taskID": 712,
    "taskName": "Madden",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 712,
    "progress": 712
  },
  {
    "taskID": 713,
    "taskName": "Tameka",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 713,
    "progress": 713
  },
  {
    "taskID": 714,
    "taskName": "Hines",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 714,
    "progress": 714
  },
  {
    "taskID": 715,
    "taskName": "Abby",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 715,
    "progress": 715
  },
  {
    "taskID": 716,
    "taskName": "Juana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 716,
    "progress": 716
  },
  {
    "taskID": 717,
    "taskName": "Maxine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 717,
    "progress": 717
  },
  {
    "taskID": 718,
    "taskName": "Susanne",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 718,
    "progress": 718
  },
  {
    "taskID": 719,
    "taskName": "Shelia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 719,
    "progress": 719
  },
  {
    "taskID": 720,
    "taskName": "Kristin",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 720,
    "progress": 720
  },
  {
    "taskID": 721,
    "taskName": "Janice",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 721,
    "progress": 721
  },
  {
    "taskID": 722,
    "taskName": "Mabel",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 722,
    "progress": 722
  },
  {
    "taskID": 723,
    "taskName": "Hilary",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 723,
    "progress": 723
  },
  {
    "taskID": 724,
    "taskName": "Hansen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 724,
    "progress": 724
  },
  {
    "taskID": 725,
    "taskName": "Dillard",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 725,
    "progress": 725
  },
  {
    "taskID": 726,
    "taskName": "Janette",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 726,
    "progress": 726
  },
  {
    "taskID": 727,
    "taskName": "Madge",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 727,
    "progress": 727
  },
  {
    "taskID": 728,
    "taskName": "Haley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 728,
    "progress": 728
  },
  {
    "taskID": 729,
    "taskName": "Krista",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 729,
    "progress": 729
  },
  {
    "taskID": 730,
    "taskName": "Janell",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 730,
    "progress": 730
  },
  {
    "taskID": 731,
    "taskName": "Barrera",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 731,
    "progress": 731
  },
  {
    "taskID": 732,
    "taskName": "Lydia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 732,
    "progress": 732
  },
  {
    "taskID": 733,
    "taskName": "Cherry",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 733,
    "progress": 733
  },
  {
    "taskID": 734,
    "taskName": "Ruby",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 734,
    "progress": 734
  },
  {
    "taskID": 735,
    "taskName": "Petra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 735,
    "progress": 735
  },
  {
    "taskID": 736,
    "taskName": "Robbins",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 736,
    "progress": 736
  },
  {
    "taskID": 737,
    "taskName": "Dolly",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 737,
    "progress": 737
  },
  {
    "taskID": 738,
    "taskName": "Janine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 738,
    "progress": 738
  },
  {
    "taskID": 739,
    "taskName": "Arnold",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 739,
    "progress": 739
  },
  {
    "taskID": 740,
    "taskName": "Lana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 740,
    "progress": 740
  },
  {
    "taskID": 741,
    "taskName": "Grace",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 741,
    "progress": 741
  },
  {
    "taskID": 742,
    "taskName": "Kent",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 742,
    "progress": 742
  },
  {
    "taskID": 743,
    "taskName": "Rowena",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 743,
    "progress": 743
  },
  {
    "taskID": 744,
    "taskName": "Hamilton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 744,
    "progress": 744
  },
  {
    "taskID": 745,
    "taskName": "Peterson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 745,
    "progress": 745
  },
  {
    "taskID": 746,
    "taskName": "Esmeralda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 746,
    "progress": 746
  },
  {
    "taskID": 747,
    "taskName": "Roberta",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 747,
    "progress": 747
  },
  {
    "taskID": 748,
    "taskName": "Estela",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 748,
    "progress": 748
  },
  {
    "taskID": 749,
    "taskName": "Carrie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 749,
    "progress": 749
  },
  {
    "taskID": 750,
    "taskName": "Good",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 750,
    "progress": 750
  },
  {
    "taskID": 751,
    "taskName": "Fitzpatrick",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 751,
    "progress": 751
  },
  {
    "taskID": 752,
    "taskName": "Fannie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 752,
    "progress": 752
  },
  {
    "taskID": 753,
    "taskName": "Montoya",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 753,
    "progress": 753
  },
  {
    "taskID": 754,
    "taskName": "Reed",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 754,
    "progress": 754
  },
  {
    "taskID": 755,
    "taskName": "Brandi",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 755,
    "progress": 755
  },
  {
    "taskID": 756,
    "taskName": "Allison",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 756,
    "progress": 756
  },
  {
    "taskID": 757,
    "taskName": "Phoebe",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 757,
    "progress": 757
  },
  {
    "taskID": 758,
    "taskName": "Buckley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 758,
    "progress": 758
  },
  {
    "taskID": 759,
    "taskName": "Dixon",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 759,
    "progress": 759
  },
  {
    "taskID": 760,
    "taskName": "Guzman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 760,
    "progress": 760
  },
  {
    "taskID": 761,
    "taskName": "Jeannette",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 761,
    "progress": 761
  },
  {
    "taskID": 762,
    "taskName": "Gertrude",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 762,
    "progress": 762
  },
  {
    "taskID": 763,
    "taskName": "Vilma",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 763,
    "progress": 763
  },
  {
    "taskID": 764,
    "taskName": "Munoz",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 764,
    "progress": 764
  },
  {
    "taskID": 765,
    "taskName": "Dudley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 765,
    "progress": 765
  },
  {
    "taskID": 766,
    "taskName": "Betty",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 766,
    "progress": 766
  },
  {
    "taskID": 767,
    "taskName": "Burks",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 767,
    "progress": 767
  },
  {
    "taskID": 768,
    "taskName": "Karla",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 768,
    "progress": 768
  },
  {
    "taskID": 769,
    "taskName": "Jenna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 769,
    "progress": 769
  },
  {
    "taskID": 770,
    "taskName": "Mckee",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 770,
    "progress": 770
  },
  {
    "taskID": 771,
    "taskName": "Lawrence",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 771,
    "progress": 771
  },
  {
    "taskID": 772,
    "taskName": "Jennifer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 772,
    "progress": 772
  },
  {
    "taskID": 773,
    "taskName": "Le",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 773,
    "progress": 773
  },
  {
    "taskID": 774,
    "taskName": "Mills",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 774,
    "progress": 774
  },
  {
    "taskID": 775,
    "taskName": "Mcdonald",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 775,
    "progress": 775
  },
  {
    "taskID": 776,
    "taskName": "Chandra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 776,
    "progress": 776
  },
  {
    "taskID": 777,
    "taskName": "Darla",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 777,
    "progress": 777
  },
  {
    "taskID": 778,
    "taskName": "Becker",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 778,
    "progress": 778
  },
  {
    "taskID": 779,
    "taskName": "Hensley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 779,
    "progress": 779
  },
  {
    "taskID": 780,
    "taskName": "Sonia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 780,
    "progress": 780
  },
  {
    "taskID": 781,
    "taskName": "Claire",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 781,
    "progress": 781
  },
  {
    "taskID": 782,
    "taskName": "Dominguez",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 782,
    "progress": 782
  },
  {
    "taskID": 783,
    "taskName": "Brittney",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 783,
    "progress": 783
  },
  {
    "taskID": 784,
    "taskName": "Fuller",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 784,
    "progress": 784
  },
  {
    "taskID": 785,
    "taskName": "Stafford",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 785,
    "progress": 785
  },
  {
    "taskID": 786,
    "taskName": "Paula",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 786,
    "progress": 786
  },
  {
    "taskID": 787,
    "taskName": "Greer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 787,
    "progress": 787
  },
  {
    "taskID": 788,
    "taskName": "Adeline",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 788,
    "progress": 788
  },
  {
    "taskID": 789,
    "taskName": "Mathews",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 789,
    "progress": 789
  },
  {
    "taskID": 790,
    "taskName": "Freeman",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 790,
    "progress": 790
  },
  {
    "taskID": 791,
    "taskName": "Myers",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 791,
    "progress": 791
  },
  {
    "taskID": 792,
    "taskName": "Collins",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 792,
    "progress": 792
  },
  {
    "taskID": 793,
    "taskName": "Constance",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 793,
    "progress": 793
  },
  {
    "taskID": 794,
    "taskName": "Monique",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 794,
    "progress": 794
  },
  {
    "taskID": 795,
    "taskName": "Melisa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 795,
    "progress": 795
  },
  {
    "taskID": 796,
    "taskName": "Pacheco",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 796,
    "progress": 796
  },
  {
    "taskID": 797,
    "taskName": "Barbara",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 797,
    "progress": 797
  },
  {
    "taskID": 798,
    "taskName": "Polly",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 798,
    "progress": 798
  },
  {
    "taskID": 799,
    "taskName": "Cobb",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 799,
    "progress": 799
  },
  {
    "taskID": 800,
    "taskName": "Ferguson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 800,
    "progress": 800
  },
  {
    "taskID": 801,
    "taskName": "Joan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 801,
    "progress": 801
  },
  {
    "taskID": 802,
    "taskName": "Adele",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 802,
    "progress": 802
  },
  {
    "taskID": 803,
    "taskName": "Patti",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 803,
    "progress": 803
  },
  {
    "taskID": 804,
    "taskName": "Landry",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 804,
    "progress": 804
  },
  {
    "taskID": 805,
    "taskName": "Corinne",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 805,
    "progress": 805
  },
  {
    "taskID": 806,
    "taskName": "Natalia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 806,
    "progress": 806
  },
  {
    "taskID": 807,
    "taskName": "Pope",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 807,
    "progress": 807
  },
  {
    "taskID": 808,
    "taskName": "Hayden",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 808,
    "progress": 808
  },
  {
    "taskID": 809,
    "taskName": "Valerie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 809,
    "progress": 809
  },
  {
    "taskID": 810,
    "taskName": "Andrea",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 810,
    "progress": 810
  },
  {
    "taskID": 811,
    "taskName": "Cline",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 811,
    "progress": 811
  },
  {
    "taskID": 812,
    "taskName": "Ayers",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 812,
    "progress": 812
  },
  {
    "taskID": 813,
    "taskName": "Sutton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 813,
    "progress": 813
  },
  {
    "taskID": 814,
    "taskName": "Nita",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 814,
    "progress": 814
  },
  {
    "taskID": 815,
    "taskName": "Lynch",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 815,
    "progress": 815
  },
  {
    "taskID": 816,
    "taskName": "Perkins",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 816,
    "progress": 816
  },
  {
    "taskID": 817,
    "taskName": "Hooper",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 817,
    "progress": 817
  },
  {
    "taskID": 818,
    "taskName": "Eugenia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 818,
    "progress": 818
  },
  {
    "taskID": 819,
    "taskName": "Noel",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 819,
    "progress": 819
  },
  {
    "taskID": 820,
    "taskName": "Conrad",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 820,
    "progress": 820
  },
  {
    "taskID": 821,
    "taskName": "Wendi",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 821,
    "progress": 821
  },
  {
    "taskID": 822,
    "taskName": "Amalia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 822,
    "progress": 822
  },
  {
    "taskID": 823,
    "taskName": "Cora",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 823,
    "progress": 823
  },
  {
    "taskID": 824,
    "taskName": "Smith",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 824,
    "progress": 824
  },
  {
    "taskID": 825,
    "taskName": "Stefanie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 825,
    "progress": 825
  },
  {
    "taskID": 826,
    "taskName": "Dickerson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 826,
    "progress": 826
  },
  {
    "taskID": 827,
    "taskName": "Bertie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 827,
    "progress": 827
  },
  {
    "taskID": 828,
    "taskName": "Ana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 828,
    "progress": 828
  },
  {
    "taskID": 829,
    "taskName": "Tammi",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 829,
    "progress": 829
  },
  {
    "taskID": 830,
    "taskName": "Neva",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 830,
    "progress": 830
  },
  {
    "taskID": 831,
    "taskName": "Beard",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 831,
    "progress": 831
  },
  {
    "taskID": 832,
    "taskName": "Sharlene",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 832,
    "progress": 832
  },
  {
    "taskID": 833,
    "taskName": "Hatfield",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 833,
    "progress": 833
  },
  {
    "taskID": 834,
    "taskName": "Koch",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 834,
    "progress": 834
  },
  {
    "taskID": 835,
    "taskName": "Mindy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 835,
    "progress": 835
  },
  {
    "taskID": 836,
    "taskName": "Ladonna",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 836,
    "progress": 836
  },
  {
    "taskID": 837,
    "taskName": "Waller",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 837,
    "progress": 837
  },
  {
    "taskID": 838,
    "taskName": "Weeks",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 838,
    "progress": 838
  },
  {
    "taskID": 839,
    "taskName": "Herring",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 839,
    "progress": 839
  },
  {
    "taskID": 840,
    "taskName": "Hester",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 840,
    "progress": 840
  },
  {
    "taskID": 841,
    "taskName": "Norris",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 841,
    "progress": 841
  },
  {
    "taskID": 842,
    "taskName": "Luisa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 842,
    "progress": 842
  },
  {
    "taskID": 843,
    "taskName": "Melissa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 843,
    "progress": 843
  },
  {
    "taskID": 844,
    "taskName": "Barbra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 844,
    "progress": 844
  },
  {
    "taskID": 845,
    "taskName": "Etta",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 845,
    "progress": 845
  },
  {
    "taskID": 846,
    "taskName": "Sharp",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 846,
    "progress": 846
  },
  {
    "taskID": 847,
    "taskName": "Trujillo",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 847,
    "progress": 847
  },
  {
    "taskID": 848,
    "taskName": "Lela",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 848,
    "progress": 848
  },
  {
    "taskID": 849,
    "taskName": "Moreno",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 849,
    "progress": 849
  },
  {
    "taskID": 850,
    "taskName": "Della",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 850,
    "progress": 850
  },
  {
    "taskID": 851,
    "taskName": "Cannon",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 851,
    "progress": 851
  },
  {
    "taskID": 852,
    "taskName": "Cathleen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 852,
    "progress": 852
  },
  {
    "taskID": 853,
    "taskName": "Roth",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 853,
    "progress": 853
  },
  {
    "taskID": 854,
    "taskName": "Fields",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 854,
    "progress": 854
  },
  {
    "taskID": 855,
    "taskName": "Mayer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 855,
    "progress": 855
  },
  {
    "taskID": 856,
    "taskName": "Jacklyn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 856,
    "progress": 856
  },
  {
    "taskID": 857,
    "taskName": "Murray",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 857,
    "progress": 857
  },
  {
    "taskID": 858,
    "taskName": "Mcmahon",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 858,
    "progress": 858
  },
  {
    "taskID": 859,
    "taskName": "Margo",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 859,
    "progress": 859
  },
  {
    "taskID": 860,
    "taskName": "Whitney",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 860,
    "progress": 860
  },
  {
    "taskID": 861,
    "taskName": "Rena",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 861,
    "progress": 861
  },
  {
    "taskID": 862,
    "taskName": "Lucille",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 862,
    "progress": 862
  },
  {
    "taskID": 863,
    "taskName": "Lawson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 863,
    "progress": 863
  },
  {
    "taskID": 864,
    "taskName": "Ellis",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 864,
    "progress": 864
  },
  {
    "taskID": 865,
    "taskName": "Karin",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 865,
    "progress": 865
  },
  {
    "taskID": 866,
    "taskName": "Stephens",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 866,
    "progress": 866
  },
  {
    "taskID": 867,
    "taskName": "Claudia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 867,
    "progress": 867
  },
  {
    "taskID": 868,
    "taskName": "Francine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 868,
    "progress": 868
  },
  {
    "taskID": 869,
    "taskName": "Tate",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 869,
    "progress": 869
  },
  {
    "taskID": 870,
    "taskName": "Cheryl",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 870,
    "progress": 870
  },
  {
    "taskID": 871,
    "taskName": "Dana",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 871,
    "progress": 871
  },
  {
    "taskID": 872,
    "taskName": "Georgette",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 872,
    "progress": 872
  },
  {
    "taskID": 873,
    "taskName": "Laura",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 873,
    "progress": 873
  },
  {
    "taskID": 874,
    "taskName": "Owens",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 874,
    "progress": 874
  },
  {
    "taskID": 875,
    "taskName": "Yates",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 875,
    "progress": 875
  },
  {
    "taskID": 876,
    "taskName": "Cantu",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 876,
    "progress": 876
  },
  {
    "taskID": 877,
    "taskName": "Cecile",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 877,
    "progress": 877
  },
  {
    "taskID": 878,
    "taskName": "Nicole",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 878,
    "progress": 878
  },
  {
    "taskID": 879,
    "taskName": "Gracie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 879,
    "progress": 879
  },
  {
    "taskID": 880,
    "taskName": "Lucia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 880,
    "progress": 880
  },
  {
    "taskID": 881,
    "taskName": "Teri",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 881,
    "progress": 881
  },
  {
    "taskID": 882,
    "taskName": "Jimmie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 882,
    "progress": 882
  },
  {
    "taskID": 883,
    "taskName": "Billie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 883,
    "progress": 883
  },
  {
    "taskID": 884,
    "taskName": "Craft",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 884,
    "progress": 884
  },
  {
    "taskID": 885,
    "taskName": "Patterson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 885,
    "progress": 885
  },
  {
    "taskID": 886,
    "taskName": "Rebecca",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 886,
    "progress": 886
  },
  {
    "taskID": 887,
    "taskName": "Olsen",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 887,
    "progress": 887
  },
  {
    "taskID": 888,
    "taskName": "June",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 888,
    "progress": 888
  },
  {
    "taskID": 889,
    "taskName": "Sargent",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 889,
    "progress": 889
  },
  {
    "taskID": 890,
    "taskName": "Alicia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 890,
    "progress": 890
  },
  {
    "taskID": 891,
    "taskName": "Maxwell",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 891,
    "progress": 891
  },
  {
    "taskID": 892,
    "taskName": "Wilson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 892,
    "progress": 892
  },
  {
    "taskID": 893,
    "taskName": "Norma",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 893,
    "progress": 893
  },
  {
    "taskID": 894,
    "taskName": "Kemp",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 894,
    "progress": 894
  },
  {
    "taskID": 895,
    "taskName": "Roseann",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 895,
    "progress": 895
  },
  {
    "taskID": 896,
    "taskName": "Riley",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 896,
    "progress": 896
  },
  {
    "taskID": 897,
    "taskName": "Gray",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 897,
    "progress": 897
  },
  {
    "taskID": 898,
    "taskName": "Tasha",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 898,
    "progress": 898
  },
  {
    "taskID": 899,
    "taskName": "Avery",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 899,
    "progress": 899
  },
  {
    "taskID": 900,
    "taskName": "Mayo",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 900,
    "progress": 900
  },
  {
    "taskID": 901,
    "taskName": "Snow",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 901,
    "progress": 901
  },
  {
    "taskID": 902,
    "taskName": "Garcia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 902,
    "progress": 902
  },
  {
    "taskID": 903,
    "taskName": "Rojas",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 903,
    "progress": 903
  },
  {
    "taskID": 904,
    "taskName": "Richardson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 904,
    "progress": 904
  },
  {
    "taskID": 905,
    "taskName": "Erickson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 905,
    "progress": 905
  },
  {
    "taskID": 906,
    "taskName": "Earline",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 906,
    "progress": 906
  },
  {
    "taskID": 907,
    "taskName": "Flynn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 907,
    "progress": 907
  },
  {
    "taskID": 908,
    "taskName": "Warren",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 908,
    "progress": 908
  },
  {
    "taskID": 909,
    "taskName": "Madeleine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 909,
    "progress": 909
  },
  {
    "taskID": 910,
    "taskName": "Sparks",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 910,
    "progress": 910
  },
  {
    "taskID": 911,
    "taskName": "Corina",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 911,
    "progress": 911
  },
  {
    "taskID": 912,
    "taskName": "Carson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 912,
    "progress": 912
  },
  {
    "taskID": 913,
    "taskName": "Alisha",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 913,
    "progress": 913
  },
  {
    "taskID": 914,
    "taskName": "Anne",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 914,
    "progress": 914
  },
  {
    "taskID": 915,
    "taskName": "Natasha",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 915,
    "progress": 915
  },
  {
    "taskID": 916,
    "taskName": "Garrison",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 916,
    "progress": 916
  },
  {
    "taskID": 917,
    "taskName": "Sheree",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 917,
    "progress": 917
  },
  {
    "taskID": 918,
    "taskName": "Obrien",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 918,
    "progress": 918
  },
  {
    "taskID": 919,
    "taskName": "Paulette",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 919,
    "progress": 919
  },
  {
    "taskID": 920,
    "taskName": "Stout",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 920,
    "progress": 920
  },
  {
    "taskID": 921,
    "taskName": "Esperanza",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 921,
    "progress": 921
  },
  {
    "taskID": 922,
    "taskName": "Key",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 922,
    "progress": 922
  },
  {
    "taskID": 923,
    "taskName": "Marilyn",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 923,
    "progress": 923
  },
  {
    "taskID": 924,
    "taskName": "Tabitha",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 924,
    "progress": 924
  },
  {
    "taskID": 925,
    "taskName": "Chang",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 925,
    "progress": 925
  },
  {
    "taskID": 926,
    "taskName": "Kasey",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 926,
    "progress": 926
  },
  {
    "taskID": 927,
    "taskName": "Cornelia",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 927,
    "progress": 927
  },
  {
    "taskID": 928,
    "taskName": "Nash",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 928,
    "progress": 928
  },
  {
    "taskID": 929,
    "taskName": "Blake",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 929,
    "progress": 929
  },
  {
    "taskID": 930,
    "taskName": "Lisa",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 930,
    "progress": 930
  },
  {
    "taskID": 931,
    "taskName": "Guerra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 931,
    "progress": 931
  },
  {
    "taskID": 932,
    "taskName": "Cassandra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 932,
    "progress": 932
  },
  {
    "taskID": 933,
    "taskName": "Lamb",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 933,
    "progress": 933
  },
  {
    "taskID": 934,
    "taskName": "Camacho",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 934,
    "progress": 934
  },
  {
    "taskID": 935,
    "taskName": "Leona",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 935,
    "progress": 935
  },
  {
    "taskID": 936,
    "taskName": "Frances",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 936,
    "progress": 936
  },
  {
    "taskID": 937,
    "taskName": "Reba",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 937,
    "progress": 937
  },
  {
    "taskID": 938,
    "taskName": "Kane",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 938,
    "progress": 938
  },
  {
    "taskID": 939,
    "taskName": "Lorrie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 939,
    "progress": 939
  },
  {
    "taskID": 940,
    "taskName": "Castaneda",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 940,
    "progress": 940
  },
  {
    "taskID": 941,
    "taskName": "Kendra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 941,
    "progress": 941
  },
  {
    "taskID": 942,
    "taskName": "Amy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 942,
    "progress": 942
  },
  {
    "taskID": 943,
    "taskName": "Suarez",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 943,
    "progress": 943
  },
  {
    "taskID": 944,
    "taskName": "Diann",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 944,
    "progress": 944
  },
  {
    "taskID": 945,
    "taskName": "Penny",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 945,
    "progress": 945
  },
  {
    "taskID": 946,
    "taskName": "Mccarthy",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 946,
    "progress": 946
  },
  {
    "taskID": 947,
    "taskName": "Morgan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 947,
    "progress": 947
  },
  {
    "taskID": 948,
    "taskName": "Bonner",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 948,
    "progress": 948
  },
  {
    "taskID": 949,
    "taskName": "Marks",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 949,
    "progress": 949
  },
  {
    "taskID": 950,
    "taskName": "Serena",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 950,
    "progress": 950
  },
  {
    "taskID": 951,
    "taskName": "Robbie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 951,
    "progress": 951
  },
  {
    "taskID": 952,
    "taskName": "Burt",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 952,
    "progress": 952
  },
  {
    "taskID": 953,
    "taskName": "Wiggins",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 953,
    "progress": 953
  },
  {
    "taskID": 954,
    "taskName": "Nichols",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 954,
    "progress": 954
  },
  {
    "taskID": 955,
    "taskName": "Odom",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 955,
    "progress": 955
  },
  {
    "taskID": 956,
    "taskName": "Bush",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 956,
    "progress": 956
  },
  {
    "taskID": 957,
    "taskName": "Byrd",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 957,
    "progress": 957
  },
  {
    "taskID": 958,
    "taskName": "Carol",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 958,
    "progress": 958
  },
  {
    "taskID": 959,
    "taskName": "Ramona",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 959,
    "progress": 959
  },
  {
    "taskID": 960,
    "taskName": "Dale",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 960,
    "progress": 960
  },
  {
    "taskID": 961,
    "taskName": "Rivers",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 961,
    "progress": 961
  },
  {
    "taskID": 962,
    "taskName": "Walls",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 962,
    "progress": 962
  },
  {
    "taskID": 963,
    "taskName": "Carroll",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 963,
    "progress": 963
  },
  {
    "taskID": 964,
    "taskName": "Silva",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 964,
    "progress": 964
  },
  {
    "taskID": 965,
    "taskName": "Beryl",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 965,
    "progress": 965
  },
  {
    "taskID": 966,
    "taskName": "Ratliff",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 966,
    "progress": 966
  },
  {
    "taskID": 967,
    "taskName": "Thomas",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 967,
    "progress": 967
  },
  {
    "taskID": 968,
    "taskName": "Kim",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 968,
    "progress": 968
  },
  {
    "taskID": 969,
    "taskName": "Gail",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 969,
    "progress": 969
  },
  {
    "taskID": 970,
    "taskName": "Farrell",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 970,
    "progress": 970
  },
  {
    "taskID": 971,
    "taskName": "Bryan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 971,
    "progress": 971
  },
  {
    "taskID": 972,
    "taskName": "Wise",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 972,
    "progress": 972
  },
  {
    "taskID": 973,
    "taskName": "Maude",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 973,
    "progress": 973
  },
  {
    "taskID": 974,
    "taskName": "Meyer",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 974,
    "progress": 974
  },
  {
    "taskID": 975,
    "taskName": "Rosario",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 975,
    "progress": 975
  },
  {
    "taskID": 976,
    "taskName": "Robinson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 976,
    "progress": 976
  },
  {
    "taskID": 977,
    "taskName": "Dee",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 977,
    "progress": 977
  },
  {
    "taskID": 978,
    "taskName": "Brandie",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 978,
    "progress": 978
  },
  {
    "taskID": 979,
    "taskName": "Lancaster",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 979,
    "progress": 979
  },
  {
    "taskID": 980,
    "taskName": "Ola",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 980,
    "progress": 980
  },
  {
    "taskID": 981,
    "taskName": "Levine",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 981,
    "progress": 981
  },
  {
    "taskID": 982,
    "taskName": "Nelson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 982,
    "progress": 982
  },
  {
    "taskID": 983,
    "taskName": "Carter",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 983,
    "progress": 983
  },
  {
    "taskID": 984,
    "taskName": "Charles",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 984,
    "progress": 984
  },
  {
    "taskID": 985,
    "taskName": "Shannon",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 985,
    "progress": 985
  },
  {
    "taskID": 986,
    "taskName": "Trevino",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 986,
    "progress": 986
  },
  {
    "taskID": 987,
    "taskName": "Contreras",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 987,
    "progress": 987
  },
  {
    "taskID": 988,
    "taskName": "Ada",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 988,
    "progress": 988
  },
  {
    "taskID": 989,
    "taskName": "Susan",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 989,
    "progress": 989
  },
  {
    "taskID": 990,
    "taskName": "Pearson",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 990,
    "progress": 990
  },
  {
    "taskID": 991,
    "taskName": "Faulkner",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 991,
    "progress": 991
  },
  {
    "taskID": 992,
    "taskName": "Madeline",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 992,
    "progress": 992
  },
  {
    "taskID": 993,
    "taskName": "Hinton",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 993,
    "progress": 993
  },
  {
    "taskID": 994,
    "taskName": "Terra",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 994,
    "progress": 994
  },
  {
    "taskID": 995,
    "taskName": "Mcclain",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 995,
    "progress": 995
  },
  {
    "taskID": 996,
    "taskName": "Bender",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 996,
    "progress": 996
  },
  {
    "taskID": 997,
    "taskName": "Gilliam",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 997,
    "progress": 997
  },
  {
    "taskID": 998,
    "taskName": "Garner",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 998,
    "progress": 998
  },
  {
    "taskID": 999,
    "taskName": "Melendez",
    "startDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "endDate": "Sat Oct 09 2021 00:28:01 GMT+0500 (Pakistan Standard Time)",
    "duration": 999,
    "progress": 999
  }
];
