import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { datamodal } from './data-model';
@Injectable({
  providedIn: 'root'
})
export class SharedServicesService {
    serviceIP: string = 'http://116.58.61.235:4388/api/';
  constructor(private httpclient: HttpClient) {
  }

  //Get withOut Parameter
  getData(UrlName: string): Observable<any> {
    var Url = this.serviceIP + UrlName;
    return this.httpclient.get(Url); 
  }
  

  ///Post 
  saveData(Objectclass: any, APIName: string = ''): Observable<any> {
    let headers={
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
}
    return this.httpclient.post(this.serviceIP + APIName, Objectclass,{
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  })
  
  }

  getColumns(UrlName: string): Observable<any> {
    var Url = this.serviceIP + UrlName;
    return this.httpclient.get(Url); 
  }
  

  ///Post 
  saveColumns(Objectclass: any, APIName: string = ''): Observable<any> {
 
    return this.httpclient.post(this.serviceIP + APIName, Objectclass)
  
    }
    ///Post 
    saveStyles(Objectclass: any, APIName: string = ''): Observable<any> {

        return this.httpclient.post(this.serviceIP + APIName, Objectclass)

    }
}
